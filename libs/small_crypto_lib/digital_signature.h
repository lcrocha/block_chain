#pragma once
#include <tuple>
#include "keys.h"
#include "hasher.h"

namespace small_crypto
{
	struct signature_t
	{
		const number_t r = 0u;
		const number_t s = 0u;

		signature_t(const number_t r_ = 0, const number_t s_ = 0) : r(r_), s(s_) {}
		signature_t(const signature_t& s_) : signature_t(s_.r, s_.s) {}
		void operator=(const signature_t& s_)
		{
			(number_t&)r = s_.r;
			(number_t&)s = s_.s;
		}
	};

	class digital_signature
	{
	public:
		static const private_key generate_private_key();
		static const public_key generate_public_key(const private_key x);
		static std::tuple<const private_key, const public_key> generate_keys();
		static const signature_t do_sign(const private_key& prk, const digest_t hash);
		static bool do_verify(const public_key& puk, const digest_t hash, const signature_t& s);
	};
}

