#include "pch.h"
#include "digital_signature.h"
using namespace small_crypto;

const private_key digital_signature::generate_private_key()
{
	static auto& pp = small_crypto::get_parameters();
	static auto max_distance = pp.q - 2;
	_ASSERT(max_distance > 0);
	//private key
	number_t x = (random(0, max_distance - 1)) + 1;

	return x;
}

const public_key digital_signature::generate_public_key(const private_key x)
{
	static auto& pp = small_crypto::get_parameters();
	//public key
	number_t y = modular_math::pow(pp.g, x, pp.p);

	return public_key(y);
}

std::tuple<const private_key, const public_key> digital_signature::generate_keys()
{
	auto x = generate_private_key();
	auto y = generate_public_key(x);

	return { private_key(x), public_key(y) };
}

const signature_t digital_signature::do_sign(const private_key& prk, const digest_t hash)
{
	auto pp = small_crypto::get_parameters();
	for (;;)
	{
		number_t k = random(1, pp.q - 1);
		auto r = number_t(modular_math::pow(pp.g, k, pp.p) % pp.q);
		if (0 == r)
			continue;
		auto s0 = modular_math::mul(prk, r, pp.q);
		auto s1 = modular_math::add(hash, s0, pp.q);
		auto s2 = modular_math::inverse(k, pp.q);
		auto s = modular_math::mul(s1, s2, pp.q);
		if (0 == s)
			continue;
		_ASSERT(0 < r && r < pp.q);
		_ASSERT(0 < s && s < pp.q);

		return { r, s };
	}
	_ASSERT(false);

	return {};
}

bool digital_signature::do_verify(const public_key& puk, const digest_t hash, const signature_t& s)
{
	auto pp = small_crypto::get_parameters();
	auto w = modular_math::inverse(s.s, pp.q);
	auto u0 = modular_math::mul(hash, w, pp.q);
	auto u1 = modular_math::mul(s.r, w, pp.q);
	auto v0 = modular_math::pow(pp.g, u0, pp.p);
	auto v1 = modular_math::pow(puk, u1, pp.p);
	auto v = modular_math::mul(v0, v1, pp.p) % pp.q;

	return v == s.r;
}