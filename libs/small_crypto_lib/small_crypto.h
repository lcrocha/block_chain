#pragma once
#include "modular_math.h"

namespace small_crypto
{
	struct parameters
	{
		number_t p;
		number_t q;
		number_t g;
	};

	number_t random(const number_t min, const number_t max);
	const parameters& get_parameters();
};

