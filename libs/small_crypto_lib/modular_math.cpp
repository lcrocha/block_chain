#include "pch.h"
#include "modular_math.h"
#include <crtdbg.h>
#include <exception>
#include <algorithm>
using namespace modular_math;

bool modular_math::is_prime(const number_t n)
{
	if (n <= 1)
		return false;
	if (n <= 3)
		return true;

	if (n % 2 == 0 || n % 3 == 0)
		return false;

	for (number_t i = 5; i * i <= n; i = i + 6)
		if (n % i == 0 || n % (i + 2) == 0)
			return false;

	return true;
}

number_t modular_math::add(const number_t n0, const number_t n1, const number_t mod)
{
	_ASSERT(mod > 1);
	auto r0 = n0 % mod;
	auto r1 = n1 % mod;
	while (r1 > 0)
	{
		auto d = std::min(mod - r0, r1);
		r0 = (r0 + d) % mod;
		r1 -= d;
	}
	_ASSERT(r0 < mod);

	return r0;
}

number_t modular_math::mul(const number_t n0, const number_t n1, const number_t mod)
{
	_ASSERT(mod > 1);
	auto n0_ = n0 % mod;
	auto n1_ = n1 % mod;
	number_t n = 0;
	for (number_t m = 0; m < n1_; m++)
		n = modular_math::add(n, n0_, mod);

	return n;
}

number_t modular_math::pow(const number_t x, const number_t y, const number_t p)
{
	number_t res = 1;
	auto x0 = x % p;
	if (x0 == 0)
		return 0;
	auto y0 = y;
	while (y0 > 0)
	{
		if (y0 & 1)
			res = modular_math::mul(res, x0, p);
		y0 = y0 >> 1;
		x0 = modular_math::mul(x0, x0, p);
	}

	return res;
}

//naive approach
number_t modular_math::inverse(const number_t a, const number_t m)
{
	auto a0 = a % m;
	for (number_t x = 1; x < m; x++) if (1 == (a0 * x) % m)
		return x;
	_ASSERT(false);

	return 0;
}

/*// Function to return gcd of a and b
number_t modular_math::gcd(const number_t a, const number_t b)
{
	if (a == 0)
		return b;

	return gcd(b % a, a);
}

// To compute x^y under modulo m
static number_t power(int x, const number_t y, const number_t m)
{
	if (y == 0)
		return 1;
	int p = power(x, y / 2, m) % m;
	p = (p * p) % m;

	return (y % 2 == 0) ? p : (x * p) % m;
}

// Function to find modular inverse of a under modulo m
// Assumption: m is prime
number_t modular_math::inverse(const number_t a, const number_t m)
{
	auto g = gcd(a, m);
	if (g != 1)
	{
		std::throw_with_nested("Inverse doesn't exist");
	}
	// If a and m are relatively prime, then modulo
	// inverse is a^(m-2) mode m
	return pow(a, m - 2, m);
}/**/