#pragma once
#include <inttypes.h>
typedef uint32_t number_t;

namespace modular_math
{
	//modular functions
	bool is_prime(const number_t n);
	number_t add(const number_t n0, const number_t n1, const number_t mod);
	number_t mul(const number_t n0, const number_t n1, const number_t mod);
	number_t pow(const number_t base, const number_t exp, const number_t mod);
	number_t inverse(const number_t a, const number_t m);
}
