#pragma once
#include "small_crypto.h"
#include "modular_math.h"

namespace small_crypto
{
	typedef number_t private_key;
	typedef number_t public_key;
}

