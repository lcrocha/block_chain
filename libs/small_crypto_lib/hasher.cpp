#include "pch.h"
#include "hasher.h"
#include <crtdbg.h>
#include <limits>
using namespace small_crypto;

hasher::hasher() : _h0(uint32_t(-1) >> 1)
{
	_ASSERT(modular_math::is_prime(_h0));
}

void hasher::update(const uint8_t n)
{
	update(&n, 1);
}

void hasher::update(const uint8_t* buff, const size_t size)
{
	_ASSERT(buff != nullptr);
	for (uint32_t n = 0; n < size; n++)
	{
		_h0 ^= std::hash<uint8_t>()(buff[n]) + (0x9e3779b9) + (_h0 << 6) + (_h0 >> 2);
	}
}

digest_t hasher::get_digest() const
{
	return _h0;
}

