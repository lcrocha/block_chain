#pragma once
#include "modular_math.h"

namespace small_crypto
{
	typedef uint32_t digest_t;

	class hasher
	{
	private:
		digest_t _h0;
	public:
		hasher();
		void update(const uint8_t n);
		template <typename B, typename S> void update(const B* buff, const S size) { update(reinterpret_cast<const uint8_t*>(buff), size_t(size)); }
		void update(const uint8_t* buff, const size_t size);
		digest_t get_digest() const;
	};
}

