#include "pch.h"
#include "small_crypto.h"
#include <random>
using namespace small_crypto;

number_t small_crypto::random(const number_t min, const number_t max)
{
	std::random_device rd;
	//std::mt19937 gen(rd());
	static std::default_random_engine gen(rd());
	std::uniform_int_distribution<number_t> dist(min, max);

	return dist(gen);
}

static bool validate_parameters(const parameters& pp)
{
	if (!modular_math::is_prime(pp.q))
		return false;
	if (!modular_math::is_prime(pp.p))
		return false;

	return pp.g > 0;
}

static bool generate_parameters(const number_t prime_seed, parameters& pp)
{
	//checking if it is prime
	if (!modular_math::is_prime(prime_seed))
		return false;
	//q is prime
	auto q0 = prime_seed;
	//select a prime p such that p - 1 is a multiple of q
	number_t p0;
	for (number_t i = 1;; i++)
	{
		p0 = modular_math::mul(q0, i, std::numeric_limits<number_t>::max()) + 1;
		if (!modular_math::is_prime(p0))
			continue;
		_ASSERT(p0 > q0);
		break;
	}
	//choose an integer h randomly from {2...p - 2}
	auto h = random(2, p0 - 2);
	//compute g = h ^ (p-1) / q mod p
	auto exp = (p0 - 1) / q0;
	if (0 == exp)
		return false;
	auto g0 = modular_math::pow(h, exp, p0);
	_ASSERT(g0 > 1);
	//all parameters found
	pp.q = q0;
	pp.p = p0;
	pp.g = g0;

	return true;
}

const parameters& small_crypto::get_parameters()
{
	static parameters pp = {};
	static bool _initialized = false;
	//checking if it is already initialized
	if (!_initialized)
	{
		//generating parameters
		number_t prime = 0;
		for (prime = 0x2fff; prime > 0; prime--) if (modular_math::is_prime(prime))
			break;
		/*while (!modular_math::is_prime(prime))
			prime = random(0xf, 0xfff);*/
		auto r = generate_parameters(prime, pp);
		_initialized = true;
	}

	return pp;
}
