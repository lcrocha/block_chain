#pragma once
#include "../small_crypto_lib/keys.h"
#include <tuple>
#include <memory>

namespace block_chain
{
	class wallet_t
	{
	private:
		const small_crypto::private_key _prk;
		mutable std::shared_ptr<small_crypto::public_key> _puk;
	public:
		wallet_t(const wallet_t& wallet);
		wallet_t(small_crypto::private_key x);
		wallet_t(const std::tuple<small_crypto::private_key, small_crypto::public_key>& keys);
		//gets
		const small_crypto::private_key get_private() const;
		const small_crypto::public_key get_public() const;
	};

	const wallet_t generate_wallet_lazy();
	const wallet_t generate_wallet();
	const wallet_t generate_wallet_by_private_key(const small_crypto::private_key prk);
}