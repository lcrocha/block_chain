#pragma once
#include "transaction.h"

namespace block_chain
{
	typedef small_crypto::digest_t difficulty_t;
	struct block_t
	{
		small_crypto::digest_t last_block_digest;
		difficulty_t difficulty;
		number_t block_number;
		tick_t tick;
		number_t nonce;
		genesis_transaction_t genesis_transaction;
		transactions_t transactions;
		small_crypto::digest_t block_digest;
	};
	typedef std::vector<block_t> blocks_t;

	block_t generate_genesis_block(const small_crypto::public_key& wallet_destination, const difficulty_t difficulty, const tick_t tick, const number_t nonce, const transactions_t& transactions);
	block_t generate_block(const small_crypto::public_key& wallet_destination, const block_t& last_block, const difficulty_t difficulty, const tick_t tick, const number_t nonce, const transactions_t& transactions);
	bool check_block(const block_t& block, bool check_signatures = true);
	bool check_block(const block_t& last_block, const block_t& block, bool check_signatures = true);
}