#pragma once
#include <list>
#include <mutex>
#include <cinttypes>

namespace block_chain
{
	template <typename T>
	class concurrent_queue
	{
	private:
		mutable std::mutex _m;
		std::list<T> _q;
	public:
		concurrent_queue() {}
		virtual ~concurrent_queue() {}
		//Operations
		template<class... _T>
		void push(_T&&... t)
		{
			std::lock_guard<std::mutex> l(_m);
			_q.emplace_back(std::forward<_T>(t)...);
		}

		bool pop(T& t)
		{
			std::lock_guard<std::mutex> l(_m);
			if (_q.empty())
				return false;
			t = std::move(_q.front());
			_q.pop_front();
			return true;
		}

		bool empty() const
		{
			std::lock_guard<std::mutex> l(_m);
			return _q.empty();
		}

		size_t size() const
		{
			std::lock_guard<std::mutex> l(_m);
			return size_t(_q.size());
		}

		void clear()
		{
			std::lock_guard<std::mutex> l(_m);
			_q.clear();
		}
	};
}