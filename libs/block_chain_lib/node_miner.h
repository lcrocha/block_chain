#pragma once
#include "node.h"

namespace block_chain
{
	class node_miner_t : public node_t
	{
	private:
		//wallet
		const small_crypto::public_key _wallet_destination;
		//difficulty
		float _difficulty_change;
		bool _is_mining;
		difficulty_t _last_difficulty;
		number_t _difficulty_block_set;
		transactions_t _pending_transactions;
		//auxilliary
		bool calculate_difficulty(difficulty_t& difficulty);
		void new_block_provided(const block_t& new_block);
	public:
		node_miner_t(const uint32_t node_id, const small_crypto::public_key& wallet_destination);
		//data
		const transactions_t& get_pending_transactions() const;
		size_t get_pending_transactions_count() const;
		small_crypto::public_key get_wallet_destination() const;
		difficulty_t get_last_difficulty() const;
		float get_difficulty_change() const;
		bool is_mining() const;
		//hooks
		virtual void on_tick(const tick_t tick) override;
		virtual void on_block_provided(const block_t& block) override;
		virtual void on_broadcast_new_block(const block_t& new_block) override;
		virtual void on_injected_transactions(const transactions_t& tt) override;
		virtual void on_wallet_funds_updated(const wallet_funds_t& wallet_funds) override;
		//operations
		virtual void release() override;
	};
}

