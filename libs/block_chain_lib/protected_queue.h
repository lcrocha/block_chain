#pragma once
#include <mutex>
#include <queue>

namespace block_chain
{
	template <typename T>
	class protected_queue_t
	{
	private:
		std::mutex _m;
		std::queue<T> _q;
	public:
		protected_queue_t() {}
		void push(T&& d)
		{
			std::lock_guard<std::mutex> l(_m);
			_q.push(std::move(d));
		}

		bool pop(T& d)
		{
			std::lock_guard<std::mutex> l(_m);
			if (_q.empty())
				return false;
			d = _q.front();
			_q.pop();

			return true;
		}
	};
}