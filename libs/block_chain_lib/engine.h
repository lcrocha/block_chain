#pragma once
#include "node_miner.h"
#include "engine_comm.h"
#include "engine_processor.h"
#include <vector>
#include <functional>
#include <memory>

namespace block_chain
{
	typedef std::unique_ptr<node_t, void(*)(node_t*)> node_storage_t;
	typedef std::vector<node_storage_t> nodes_t;

	struct node_info_t
	{
		node_type_t node_type;
		uint32_t node_id;
		number_t last_block_number;
		uint32_t blocks_count;
		size_t pending_transactions_count;
		difficulty_t difficulty;
		float difficulty_change;
		bool is_mining;
		small_crypto::digest_t last_block_digest;
		small_crypto::public_key wallet_destination;
		node_info_t(const node_t& node) : node_id(node.get_node_id()), blocks_count(node.get_blocks_count()), node_type(node.get_node_type()), difficulty(0u), difficulty_change(1.0f), last_block_digest(0u), last_block_number(node.get_last_block_number()), is_mining(false), pending_transactions_count(), wallet_destination()
		{
			if (node_type_t::miner != node_type)
				return;
			auto& miner = (node_miner_t&)node;
			difficulty = miner.get_last_difficulty();
			difficulty_change = miner.get_difficulty_change();
			if (node.get_blocks_count() == 0)
				return;
			last_block_digest = miner.get_last_block_digest();
			is_mining = miner.is_mining();
			pending_transactions_count = miner.get_pending_transactions_count();
			wallet_destination = miner.get_wallet_destination();
		}
	};
	typedef std::function<void(const node_info_t&)> browse_nodes_function;

	struct consensus_info_t
	{
		wallet_funds_t wallet_funds;
		block_t block;
		std::vector<node_id_t> node_ids;
	};

	typedef std::map<small_crypto::digest_t, consensus_info_t> consensus_map_t;

	class engine_t : public engine_comm_t, public engine_processor_t
	{
	private:
		uint32_t _ticks = 0;
		nodes_t _nodes;
		messages_t _messages;
		bool _parallel;
		size_t _last_max_pending_transactions;
		//consensus
		consensus_info_t* _last_consensus;
		consensus_map_t _last_consensus_list;
		//auxilliary
		bool query_node(comm_command_storage_t& command, node_t*& node);
		bool query_block(comm_command_storage_t& command, block_t* block);
		void process_comm_commands();
		void process_processor_commands();
	public:
		engine_t(const uint32_t port_number);
		virtual ~engine_t();
		//nodes
		void add_node();
		node_id_t add_miner(const small_crypto::public_key& wallet_destination);
		void browse_nodes(const browse_nodes_function&& f);
		//transactions
		void add_transaction(const wallet_t& source, const small_crypto::public_key& destination, const coins_amount_t amount, const coins_amount_t fee);
		//status
		const consensus_info_t* const get_last_consensus() const;
		const consensus_map_t& get_last_consensus_list() const;
		size_t get_max_pending_transactions() const;
		size_t get_nodes_count() const;
		size_t get_ticks_count() const;
		//parallel
		void toggle_parallel();
		bool is_parallel();
		//operation
		void do_tick();
	};
}
