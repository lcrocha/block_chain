#pragma once
#include "concurrent_queue.h"
#include "transaction.h"
#include <thread>
//#include <memory>

namespace block_chain
{
	enum class processor_command_type_t { sign_transaction };
	struct processor_command_t
	{
		const processor_command_type_t type;
		processor_command_t(const processor_command_type_t& type_) : type(type_) {}
		virtual void release() = 0;
	};
	typedef std::shared_ptr<processor_command_t> processor_command_storage_t;

	struct processor_sign_transaction_t : processor_command_t
	{
		const wallet_t source;
		const small_crypto::public_key destination;
		const coins_amount_t amount;
		const coins_amount_t fee;
		const tick_t tick;
		std::unique_ptr<transaction_t> transaction;
		processor_sign_transaction_t(const wallet_t& s, const small_crypto::public_key& d, const coins_amount_t a, const coins_amount_t f, const tick_t t) : processor_command_t(processor_command_type_t::sign_transaction),
		source(s), destination(d), amount(a), fee(f), tick(t) {}
		virtual void release() override { delete this; }
	};

	class engine_processor_t
	{
	private:
		//queue
		concurrent_queue<processor_command_storage_t> _to_process;
		concurrent_queue<processor_command_storage_t> _processed;
		//processor
		bool _working;
		std::thread _thread;
		//processor thread
		void processor_execution();
	public:
		engine_processor_t();
		virtual ~engine_processor_t();
		//queues
		size_t get_to_process_count() const;
		size_t get_processed_count() const;
		//operations
		bool pop_command(processor_command_storage_t& processor_command);
		void do_sign_transaction(const wallet_t& source, const small_crypto::public_key& destination, const coins_amount_t amount, const coins_amount_t fee, const tick_t tick);
	};
}

