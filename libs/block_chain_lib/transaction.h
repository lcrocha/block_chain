#pragma once
#include "wallet.h"
#include "../small_crypto_lib/hasher.h"
#include "../small_crypto_lib/digital_signature.h"
#include <vector>

namespace block_chain
{
	typedef int32_t coins_amount_t;
	struct genesis_transaction_t
	{
		small_crypto::public_key wallet_destination;
		coins_amount_t amount;
		small_crypto::digest_t digest;
	};

	typedef uint32_t tick_t;

	struct transaction_t
	{
		small_crypto::public_key wallet_source;
		small_crypto::public_key wallet_destination;
		coins_amount_t amount;
		coins_amount_t fee;
		tick_t tick;
		small_crypto::digest_t digest;
		small_crypto::signature_t signature;

		bool operator==(const transaction_t& t) const { return tick == t.tick && digest == t.digest; }
	};
	typedef std::vector<transaction_t> transactions_t;

	const transaction_t generate_transaction(const wallet_t& wallet_source, const small_crypto::public_key& wallet_destination, const coins_amount_t amount, const coins_amount_t fee, const tick_t tick);
	void sign_transaction(const wallet_t& wallet_source, transaction_t& t);
	bool check_transaction(const transaction_t& t, bool check_signatures = true);
	bool check_transactions(const transactions_t& tt, bool check_signatures = true);

	const genesis_transaction_t generate_genesis_transaction(const small_crypto::public_key& wallet_destination, const transactions_t& transactions);
	bool check_genesis_transaction(const genesis_transaction_t& gt);
}