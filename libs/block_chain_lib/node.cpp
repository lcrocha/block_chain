#include "pch.h"
#include "node.h"
#include "config.h"
#include <iterator>
#include <algorithm>
#include <numeric>
#include <Windows.h>
#include <sstream>
#include <iomanip>
using namespace block_chain;

static void delete_message(message_t* m)
{
	m->release();
}

node_t::node_t(const uint32_t node_id, const node_type_t node_type) : _node_type(node_type), _node_id(node_id)
{}

void node_t::release()
{
	delete this;
}

node_type_t node_t::get_node_type() const
{
	return _node_type;
}

uint32_t node_t::get_node_id() const
{
	return _node_id;
}

uint32_t node_t::get_blocks_count() const
{
	return uint32_t(_blocks.size());
}

void message_deleter(message_t* m)
{
	m->release();
}

void node_t::broadcast_new_block(const block_t& new_block)
{
	on_broadcast_new_block(new_block);
	auto message = std::shared_ptr<message_broadcast_new_block_t>(new message_broadcast_new_block_t(get_node_id(), new_block), delete_message);
	if (_messages_out.end() != std::find_if(_messages_out.begin(), _messages_out.end(), [&](auto& m) { return m->is_equal(*message); }))
		return;
	_messages_out.push_back(std::move(message));
}

void node_t::request_block(number_t bn, small_crypto::digest_t bd)
{
	auto message = std::shared_ptr<message_request_block_t>(new message_request_block_t(get_node_id(), bn, bd), delete_message);
	if (_messages_out.end() != std::find_if(_messages_out.begin(), _messages_out.end(), [&](auto& m) { return m->is_equal(*message); }))
		return;
	_messages_out.push_back(std::move(message));
}

void node_t::provide_block(const block_t& block)
{
	auto message = std::shared_ptr<message_provide_block_t>(new message_provide_block_t(get_node_id(), block), delete_message);
	if (_messages_out.end() != std::find_if(_messages_out.begin(), _messages_out.end(), [&](auto& m) { return m->is_equal(*message); }))
		return;
	_messages_out.push_back(std::move(message));
}

bool node_t::is_blocks_empty() const
{
	return 0u == get_blocks_count();
}

const block_t& node_t::get_last_block() const
{
	return _blocks.back();
}

void node_t::on_broadcast_new_block(const block_t& new_block)
{}

const blocks_t::const_reverse_iterator node_t::find_block_by_number(const number_t block_number) const
{
	return std::find_if(_blocks.rbegin(), _blocks.rend(), [block_number](const block_t& b) { return block_number == b.block_number; });
}

bool node_t::is_block_available(const number_t block_number) const
{
	return _blocks.rend() != find_block_by_number(block_number);
}

const block_t& node_t::get_block_by_number(const number_t block_number) const
{
	return *find_block_by_number(block_number);
}

small_crypto::digest_t node_t::get_last_block_digest() const
{
	return _blocks.rbegin()->block_digest;
}

number_t node_t::get_last_block_number() const
{
	return _blocks.empty() ? 0 : _blocks.rbegin()->block_number;
}

const wallet_funds_t& node_t::get_wallet_funds() const
{
	return _wallet_funds;
}

bool node_t::is_requesting_blocks() const
{
	return !is_blocks_empty() && _blocks.front().block_number != config_first_block_number;
}

check_new_block_return_t node_t::on_check_new_block(const node_id_t sender, const block_t& new_block)
{
	if (!block_chain::check_block(new_block, false))
		return check_new_block_return_t::invalid_block;
	//if (sender % 5 != get_node_id() % 5) return check_new_block_return_t::block_rejected;
	//if (0 == small_crypto::random(0, 10)) return check_new_block_return_t::block_rejected;
	if (!_blocks.empty() && !block_chain::check_block(get_last_block(), new_block, false))
		return check_new_block_return_t::invalid_sequence;

	return check_new_block_return_t::ok;
}

void node_t::add_block(const block_t& new_block)
{
	_blocks.push_back(new_block);
	update_wallet_funds(new_block);
	//executing sanity checks
	if (is_requesting_blocks())
		return;
	try
	{
		//checking by digest
		small_crypto::digest_t last_digest = new_block.last_block_digest;
		auto it = _blocks.rbegin();
		while (_blocks.rend() != ++it)
		{
			if (it->block_digest != last_digest)
				throw std::exception("block chain sequence invalid");
			last_digest = it->last_block_digest;
		}
		//checking total funds
		auto total_funds = coins_amount_t(std::accumulate(_wallet_funds.begin(), _wallet_funds.end(), 0u, [](auto a, auto b) { return a + b.second; }));
		auto coins_generated = coins_amount_t(config_coins_generated_each_block * _blocks.size());
		auto funds_diff = total_funds - coins_generated;
		if (funds_diff != 0)
			throw std::exception("total funds different than coins generated");
		//checking negative
		if (!std::none_of(_wallet_funds.begin(), _wallet_funds.end(), [](const auto& it) { return it.second < 0; }))
			throw std::exception("negative value not allowed");
	}
	catch (const std::exception& e)
	{
		SYSTEMTIME st = {};
		::GetLocalTime(&st);
		std::ostringstream oss;
		oss << std::setw(2) << std::setfill('0') << st.wHour
			<< ":" << std::setw(2) << std::setfill('0') << st.wMinute
			<< ":" << std::setw(2) << std::setfill('0') << st.wSecond
			<< " node_id: " << node_t::get_node_id() << " - " << e.what() << std::endl;
		::OutputDebugStringA(oss.str().c_str());
		_wallet_funds.clear();
		_blocks.clear();
	}
}

void node_t::do_tick(const tick_t tick, const messages_t& messages_in, messages_t& messages_out)
{
	//processing messages
	for (auto& message : messages_in)
	{
		on_message(message.get());
	}
	on_tick(tick);
	//sending messages
	if (!_messages_out.empty())
	{
		std::move(_messages_out.begin(), _messages_out.end(), std::back_inserter(messages_out));
		_messages_out.clear();
	}
}

void node_t::on_injected_transactions(const transactions_t& tt)
{
}

void node_t::on_wallet_funds_updated(const wallet_funds_t& wallet_funds)
{}

void node_t::on_block_provided(const block_t& new_block)
{}

void node_t::on_message(const message_t* message)
{
	if (get_node_id() == message->sender)
		return;
	switch (message->message_type)
	{
	case message_type_t::provide_block:
		{
			if (_blocks.empty())
				break;
			auto provide_block = (message_provide_block_t*)message;
			auto front_block_it = _blocks.begin();
			if (provide_block->block.block_number == front_block_it->block_number - 1 && provide_block->block.block_digest == front_block_it->last_block_digest)
			{
				_blocks.insert(front_block_it, provide_block->block);
				update_wallet_funds(provide_block->block);
				on_block_provided(provide_block->block);
			}
			break;
		}
	case message_type_t::broadcast_new_block:
		{
			auto broadcast_new_block = (message_broadcast_new_block_t*)message;
			auto& new_block = broadcast_new_block->block;
			auto r = on_check_new_block(message->sender, new_block);
			if (check_new_block_return_t::invalid_block == r || check_new_block_return_t::block_rejected == r)
				break;
			if (new_block.block_number <= get_last_block_number())
				break;
			if (check_new_block_return_t::invalid_sequence == r)
			{
				_wallet_funds.clear();
				_blocks.clear();
			}
			on_broadcast_new_block(new_block);
			add_block(new_block);

			break;
		}
	case message_type_t::request_block:
		{
			auto request_block = (message_request_block_t*)message;
			auto block_it = std::find_if(_blocks.begin(), _blocks.end(), [&](auto b) { return b.block_number == request_block->block_number && b.block_digest == request_block->block_digest; });
			if (block_it == _blocks.end())
				break;
			provide_block(*block_it);
			break;
		}
	case message_type_t::injected_transactions:
		{
			auto it = (message_injected_transactions_t*)message;
			on_injected_transactions(it->transactions);
			break;
		}
	default:
		_ASSERT(false);
	}
}

void node_t::update_wallet_funds(const block_t& block)
{
	std::map<small_crypto::public_key, coins_amount_t> consolidated_funds;
	consolidated_funds[block.genesis_transaction.wallet_destination] += block.genesis_transaction.amount;
	for (auto& t : block.transactions)
	{
		consolidated_funds[t.wallet_source] -= (t.amount + t.fee);
		consolidated_funds[t.wallet_destination] += t.amount;
	}
	//applying to wallet funds
	for (auto& consolidated_wallet_it : consolidated_funds)
	{
		_wallet_funds[consolidated_wallet_it.first] += consolidated_wallet_it.second;
	}
	//calling hook
	on_wallet_funds_updated(_wallet_funds);
}

void node_t::on_tick(const tick_t /*tick*/)
{
	if (is_blocks_empty() || !is_requesting_blocks())
		return;
	//request
	auto front_block_it = _blocks.begin();
	request_block(front_block_it->block_number - 1, front_block_it->last_block_digest);
}
