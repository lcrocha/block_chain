#include "pch.h"
#include "node_miner.h"
#include "config.h"
#include <iterator>
#include <algorithm>
#include <numeric>
using namespace block_chain;

node_miner_t::node_miner_t(const uint32_t node_id, const small_crypto::public_key& wallet_destination) : node_t(node_id, node_type_t::miner), _difficulty_change(0.0f), _is_mining(false), _difficulty_block_set(0u), _last_difficulty(0u), _wallet_destination(wallet_destination)
{}

void node_miner_t::release()
{
	delete this;
}

bool node_miner_t::is_mining() const
{
	return _is_mining;
}

const transactions_t& node_miner_t::get_pending_transactions() const
{
	return _pending_transactions;
}

size_t node_miner_t::get_pending_transactions_count() const
{
	return _pending_transactions.size();
}

difficulty_t node_miner_t::get_last_difficulty() const
{
	return _last_difficulty;
}

small_crypto::public_key node_miner_t::get_wallet_destination() const
{
	return _wallet_destination;
}

float node_miner_t::get_difficulty_change() const
{
	return _difficulty_change;
}

static transactions_t validate_transactions(const wallet_funds_t& wallet_funds, const transactions_t& pending_transactions)
{
	auto local_pending_transactions = pending_transactions;
	//checking valid transactions based on funds
	auto l_wallet_funds = wallet_funds;
	auto erase_it = std::remove_if(local_pending_transactions.begin(), local_pending_transactions.end(), [&](const transaction_t& t)
		{
			//check by amount
			auto to_deduct = t.amount + t.fee;
			if (l_wallet_funds[t.wallet_source] < to_deduct)
				return true;
			l_wallet_funds[t.wallet_source] -= to_deduct;
			l_wallet_funds[t.wallet_destination] += t.amount;
			return false;
		});
	if (erase_it != local_pending_transactions.end())
		local_pending_transactions.erase(erase_it, local_pending_transactions.end());

	return local_pending_transactions;
}

void node_miner_t::new_block_provided(const block_t& new_block)
{
	auto& new_pending_transactions = new_block.transactions;
	if (_pending_transactions.empty() || new_pending_transactions.empty())
		return;
	_pending_transactions.erase(std::remove_if(_pending_transactions.begin(), _pending_transactions.end(), [&](const transaction_t& t0)
		{
			return new_pending_transactions.end() != std::find(new_pending_transactions.begin(), new_pending_transactions.end(), t0);
		}), _pending_transactions.end());
}

void node_miner_t::on_block_provided(const block_t& new_block)
{
	new_block_provided(new_block);
}

void node_miner_t::on_broadcast_new_block(const block_t& new_block)
{
	new_block_provided(new_block);
}

void node_miner_t::on_injected_transactions(const transactions_t& tt)
{
	std::copy(tt.begin(), tt.end(), std::back_inserter(_pending_transactions));
	std::sort(_pending_transactions.begin(), _pending_transactions.end(), [](const auto& t0, const auto& t1) { return t0.fee > t1.fee; });
	_pending_transactions = std::move(validate_transactions(node_t::get_wallet_funds(), _pending_transactions));
}

void node_miner_t::on_wallet_funds_updated(const wallet_funds_t& wallet_funds)
{
	_pending_transactions = std::move(validate_transactions(wallet_funds, _pending_transactions));
}

bool node_miner_t::calculate_difficulty(difficulty_t& difficulty)
{
	//default difficulty for first set
	difficulty = difficulty_t(-1) >> 16;
	if (node_t::is_blocks_empty())
		return true;
	//getting last blocks difficulty
	auto& last_block = node_t::get_last_block();
	auto last_block_number = last_block.block_number;
	difficulty = last_block.difficulty;
	//time to change difficulty?
	if (0 != (last_block_number % config_blocks_to_difficulty_adjust))
		return true;
	//checking difficulty change needed
	if (0 != _last_difficulty)
		difficulty = _last_difficulty;
	auto blocks_set = last_block_number / config_blocks_to_difficulty_adjust;
	if (blocks_set <= _difficulty_block_set)
		return true;
	//calculating new difficulty
	auto last_blocks_set = (last_block_number / config_blocks_to_difficulty_adjust);
	auto first_block_number = config_first_block_number + ((last_blocks_set - 1) * config_blocks_to_difficulty_adjust);
	if (!node_t::is_block_available(first_block_number))
		return false;
	_ASSERT(last_block_number == last_block.block_number);
	auto& first_block_set = node_t::get_block_by_number(first_block_number);
	auto total_ticks = last_block.tick - first_block_set.tick;
	_difficulty_block_set = blocks_set;
	_difficulty_change = float(total_ticks) / (config_blocks_to_difficulty_adjust * config_ticks_per_block);
	_last_difficulty = difficulty = std::max(1u , difficulty_t(difficulty * _difficulty_change));

	return true;
}

void node_miner_t::on_tick(const tick_t tick)
{
	node_t::on_tick(tick);
	difficulty_t difficulty;
	_is_mining = !node_t::is_requesting_blocks() && calculate_difficulty(difficulty);
	if (!_is_mining)
		return;
	auto nonce = small_crypto::random(0, -1);
	//selecting top transactions
	auto selected_transactions = _pending_transactions;
	if (selected_transactions.size() > config_max_transactions_per_block)
		selected_transactions.resize(config_max_transactions_per_block);
	//check last block
	auto new_block = node_t::is_blocks_empty() ?
		block_chain::generate_genesis_block(_wallet_destination, difficulty, tick, nonce, selected_transactions) :
		block_chain::generate_block(_wallet_destination, node_t::get_last_block(), difficulty, tick, nonce, selected_transactions);
	if (new_block.difficulty > new_block.block_digest)
	{
		node_t::add_block(new_block);
		node_t::broadcast_new_block(new_block);
	}
}