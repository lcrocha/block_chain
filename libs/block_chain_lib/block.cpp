#include "pch.h"
#include "block.h"
#include "config.h"
using namespace block_chain;

static block_t l_generate_block(
	const small_crypto::public_key& wallet_destination,
	const small_crypto::digest_t& last_block_digest,
	const number_t block_number,
	const difficulty_t difficulty,
	const tick_t tick,
	const number_t nonce,
	const transactions_t& transactions)
{
	auto gt = generate_genesis_transaction(wallet_destination, transactions);
	auto block = block_t{ last_block_digest, difficulty, block_number, tick, nonce, gt, transactions };
	constexpr auto header_size = sizeof(block_t::last_block_digest) + sizeof(block_t::difficulty) + sizeof(block_t::block_number) + sizeof(block_t::tick) + sizeof(block_t::nonce);
	//hashing header
	small_crypto::hasher hasher;
	hasher.update(&block, header_size);
	//genesis transaction
	hasher.update(block.genesis_transaction.digest);
	//transactions
	for (auto& transaction : block.transactions)
		hasher.update(&transaction.digest, sizeof(transaction.digest));
	//updating digest
	block.block_digest = hasher.get_digest();

	return block;
}

block_t block_chain::generate_genesis_block(const small_crypto::public_key& wallet_destination, const difficulty_t difficulty, const tick_t tick, const number_t nonce, const transactions_t& transactions)
{
	return l_generate_block(wallet_destination, 0u, config_first_block_number, difficulty, tick, nonce, transactions);
}

block_t block_chain::generate_block(const small_crypto::public_key& wallet_destination, const block_t& last_block, const difficulty_t difficulty, const tick_t tick, const number_t nonce, const transactions_t& transactions)
{
	return l_generate_block(wallet_destination, last_block.block_digest, last_block.block_number + 1, difficulty, tick, nonce, transactions);
}

bool block_chain::check_block(const block_t& block, bool check_signatures)
{
	if (block.block_digest >= block.difficulty)
		return false;
	auto b0 = l_generate_block(block.genesis_transaction.wallet_destination, block.last_block_digest, block.block_number, block.difficulty, block.tick, block.nonce, block.transactions);
	if (b0.genesis_transaction.digest != block.genesis_transaction.digest)
		return false;
	if (b0.block_digest != block.block_digest)
		return false;

	return block_chain::check_transactions(block.transactions, check_signatures);
}

bool block_chain::check_block(const block_t& last_block, const block_t& next_block, bool check_signatures)
{
	if (last_block.block_number != next_block.block_number - 1)
		return false;
	if (last_block.block_digest != next_block.last_block_digest)
		return false;
	if (last_block.tick >= next_block.tick)
		return false;
	for (auto& transaction : next_block.transactions) if (transaction.tick > next_block.tick)
		return false;

	return check_block(next_block, check_signatures);
}