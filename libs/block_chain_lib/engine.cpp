#include "pch.h"
#include "engine.h"
#include "node_miner.h"
#include "../small_crypto_lib/digital_signature.h"
#include <ppl.h>
#include <string>
#include <tuple>

using namespace block_chain;

static void delete_node(node_t* n)
{
	n->release();
}

engine_t::engine_t(const uint32_t port_number) :
#ifdef _DEBUG
	_parallel(false),
#else
	_parallel(true),
#endif
	engine_comm_t(port_number),
	_last_max_pending_transactions(0),
	_last_consensus(nullptr)
{
}

engine_t::~engine_t()
{
}

void engine_t::add_node()
{
	_nodes.push_back({ new node_t(uint32_t(_nodes.size() + 1)), delete_node });
}

node_id_t engine_t::add_miner(const small_crypto::public_key& wallet_destination)
{
	_nodes.push_back({ new node_miner_t(uint32_t(_nodes.size() + 1), wallet_destination), delete_node });

	return _nodes.back()->get_node_id();
}

size_t engine_t::get_nodes_count() const
{
	return _nodes.size();
}

size_t engine_t::get_ticks_count() const
{
	return _ticks;
}

const consensus_info_t* const engine_t::get_last_consensus() const
{
	return _last_consensus;
}

const consensus_map_t& engine_t::get_last_consensus_list() const
{
	return _last_consensus_list;
}

size_t engine_t::get_max_pending_transactions() const
{
	return _last_max_pending_transactions;
}

void engine_t::browse_nodes(const browse_nodes_function&& f)
{
	for (auto& node : _nodes)
	{
		node_info_t node_info(*node);
		f(node_info);
	}
}

void engine_t::add_transaction(const wallet_t& source, const small_crypto::public_key& destination, const coins_amount_t amount, const coins_amount_t fee)
{
	engine_processor_t::do_sign_transaction(source, destination, amount, fee, _ticks);
}

void engine_t::toggle_parallel()
{
	_parallel = !_parallel;
}

bool engine_t::is_parallel()
{
	return _parallel;
}

bool engine_t::query_node(comm_command_storage_t& command, node_t*& node)
{
	//getting node id
	auto node_id_pair = command->params.find("node_id");
	auto node_id = command->params.end() == node_id_pair ? 1 : std::stol(node_id_pair->second);
	if (command->params.end() == node_id_pair)
	{
		//using consensus
		if (nullptr == _last_consensus || _last_consensus->node_ids.empty())
		{
			command->response["message"] = "no consensus available";
			command->data_ready();
			return false;
		}
		node_id = *_last_consensus->node_ids.begin();
	}
	auto node_it = std::find_if(_nodes.begin(), _nodes.end(), [node_id](const node_storage_t& node) { return node_id == node->get_node_id(); });
	command->response["_input"].push_back({ "node_id", node_id });
	if (_nodes.end() == node_it)
	{
		command->response["message"] = "node not found";
		command->data_ready();
		return false;
	}
	node = node_it->get();

	return true;
}

bool engine_t::query_block(comm_command_storage_t& command, block_t* block)
{
	node_t* node = nullptr;
	if (!query_node(command, node))
		return false;
	//getting block number
	if (0 == node->get_blocks_count())
	{
		command->response["message"] = "no blocks available";
		command->data_ready();
		return false;
	}
	auto block_number_pair = command->params.find("block_number");
	auto block_number = command->params.end() == block_number_pair ? node->get_last_block_number() : std::stol(block_number_pair->second);
	command->response["_input"].push_back({ "block_number", block_number });
	if (block_number != -1 && !node->is_block_available(block_number))
	{
		command->response["message"] = "block not available";
		command->data_ready();
		return false;
	}
	*block = (-1 == block_number ? node->get_last_block() : node->get_block_by_number(block_number));

	return true;
}

void engine_t::process_comm_commands()
{
	auto command = engine_comm_t::pop_command();
	if (!command)
		return;
	switch (command->type)
	{
	case comm_command_type_t::query_nodes:
	{
		auto nodes = nlohmann::json::array();
		browse_nodes([&](const block_chain::node_info_t& node_info)
			{
				nodes.push_back({
					{"node_id", node_info.node_id},
					{"node_is_mining", node_info.is_mining},
					{"wallet_destination", node_info.wallet_destination},
					{"last_block_digest", node_info.last_block_digest},
					{"last_block_number", node_info.last_block_number},
					{"blocks_count", node_info.blocks_count},
					{"pending_transactions_count", node_info.pending_transactions_count},
					{"difficulty", node_info.difficulty},
					{"difficulty_change", node_info.difficulty_change } });
			});
		command->response["nodes"] = nodes;
		command->response["_ticks"] = get_ticks_count();
		command->response["_nodes_count"] = get_nodes_count();
		command->response["_messages"] = _messages.size();
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_block_full:
	{
		block_t block;
		if (!query_block(command, &block))
		{
			command->data_ready();
			break;
		}
		//generating response
		command->response["last_block_digest"] = block.last_block_digest;
		command->response["difficulty"] = block.difficulty;
		command->response["block_number"] = block.block_number;
		command->response["tick"] = block.tick;
		command->response["nonce"] = block.nonce;
		//genesis transaction
		auto genesis_transaction_json = nlohmann::json();
		genesis_transaction_json["wallet_destination"] = block.genesis_transaction.wallet_destination;
		genesis_transaction_json["amount"] = block.genesis_transaction.amount;
		genesis_transaction_json["digest"] = block.genesis_transaction.digest;
		command->response["genesis_transaction"] = genesis_transaction_json;
		command->response["block_digest"] = block.block_digest;
		auto transactions_json = nlohmann::json::array();
		for (auto& t : block.transactions)
		{
			auto transaction_json = nlohmann::json();
			transaction_json["wallet_source"] = t.wallet_source;
			transaction_json["wallet_destination"] = t.wallet_destination;
			transaction_json["amount"] = t.amount;
			transaction_json["fee"] = t.fee;
			transaction_json["tick"] = t.tick;
			transaction_json["digest"] = t.digest;
			transaction_json["signature.s"] = t.signature.s;
			transaction_json["signature.r"] = t.signature.r;
			transactions_json.push_back(transaction_json);
		}
		command->response["transactions"] = transactions_json;
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_block_no_transactions:
	{
		block_t block;
		if (!query_block(command, &block))
		{
			command->data_ready();
			break;
		}
		//generating response
		command->response["last_block_digest"] = block.last_block_digest;
		command->response["difficulty"] = block.difficulty;
		command->response["block_number"] = block.block_number;
		command->response["tick"] = block.tick;
		command->response["nonce"] = block.nonce;
		//genesis transaction
		auto genesis_transaction_json = nlohmann::json();
		genesis_transaction_json["wallet_destination"] = block.genesis_transaction.wallet_destination;
		genesis_transaction_json["amount"] = block.genesis_transaction.amount;
		genesis_transaction_json["digest"] = block.genesis_transaction.digest;
		command->response["genesis_transaction"] = genesis_transaction_json;
		command->response["block_digest"] = block.block_digest;
		command->response["transactions_count"] = block.transactions.size();
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_pending_transactions:
	{
		block_chain::node_miner_t* node = nullptr;
		if (!query_node(command, (node_t*&)node))
		{
			command->data_ready();
			break;
		}
		//generating response
		auto& pending_transactions = node->get_pending_transactions();
		auto transactions_json = nlohmann::json::array();
		for (auto& t : pending_transactions)
		{
			auto transaction_json = nlohmann::json();
			transaction_json["wallet_source"] = t.wallet_source;
			transaction_json["wallet_destination"] = t.wallet_destination;
			transaction_json["amount"] = t.amount;
			transaction_json["fee"] = t.fee;
			transaction_json["tick"] = t.tick;
			transaction_json["digest"] = t.digest;
			transaction_json["signature.s"] = t.signature.s;
			transaction_json["signature.r"] = t.signature.r;
			transactions_json.push_back(transaction_json);
		}
		command->response["transactions"] = transactions_json;
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_consensus:
	{
		if (_last_consensus_list.empty())
		{
			command->response["message"] = "no consensus found";
			command->data_ready();
			break;
		}
		auto consensus_list_json = nlohmann::json::array();
		for (auto& consensus_it : _last_consensus_list)
		{
			auto consensus_json = nlohmann::json();
			auto& consensus = consensus_it.second;
			auto wallet_funds_json = nlohmann::json::array();
			for (auto& wallet_fund_it : consensus.wallet_funds)
			{
				auto wallet_fund_json = nlohmann::json();
				wallet_fund_json["wallet"] = wallet_fund_it.first;
				wallet_fund_json["amount"] = wallet_fund_it.second;
				wallet_funds_json.push_back(wallet_fund_json);
			}
			consensus_json["wallet_funds"] = wallet_funds_json;
			consensus_json["nodes_count"] = consensus.node_ids.size();
			consensus_json["block_number"] = consensus.block.block_number;
			consensus_json["block_digest"] = consensus.block.block_digest;
			consensus_json["block_difficulty"] = consensus.block.difficulty;
			consensus_list_json.push_back(consensus_json);
		}
		command->response["consensus"] = consensus_list_json;
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_transaction:
	{
		block_t block;
		if (!query_block(command, &block))
		{
			command->data_ready();
			break;
		}
		//getting transaction number
		auto transaction_number_pair = command->params.find("transaction_number");
		auto transaction_number = command->params.end() == transaction_number_pair ? 0 : std::stol(transaction_number_pair->second);
		command->response["_input"].push_back({ "transaction_number", transaction_number });
		if (transaction_number >= block.transactions.size())
		{
			command->response["message"] = "transaction not available";
			command->data_ready();
			break;
		}
		auto& transaction = block.transactions[transaction_number];
		//generating response
		command->response["wallet_source"] = transaction.wallet_source;
		command->response["wallet_destination"] = transaction.wallet_destination;
		command->response["amount"] = transaction.amount;
		command->response["fee"] = transaction.fee;
		command->response["tick"] = transaction.tick;
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_wallet_destination:
	{
		auto& params = command->params;
		//getting parameters
		auto private_key_it = params.find("private_key");
		if (params.end() == private_key_it)
		{
			command->response["message"] = "private_key not provided";
			command->data_ready();
			break;
		}
		auto private_key = std::atoi(private_key_it->second.c_str());
		if (private_key < 1)
		{
			command->response["message"] = "private_key number invalid";
			command->data_ready();
			break;
		}
		auto wallet_destination = small_crypto::digital_signature::generate_public_key(private_key);
		command->response["_message"] = "public key generated";
		command->response["_wallet_destination"] = wallet_destination;
		command->data_ready();
		break;
	}
	case comm_command_type_t::query_processor:
	{
		//generating response
		command->response["processed_count"] = engine_processor_t::get_processed_count();
		command->response["to_process_count"] = engine_processor_t::get_to_process_count();
		command->data_ready();
		break;
	}
	case comm_command_type_t::add_transaction:
	{
		auto& params = command->params;
		//getting parameters
		auto source_prk_it = params.find("source_privatekey");
		if (params.end() == source_prk_it)
		{
			command->response["message"] = "source_privatekey not provided";
			command->data_ready();
			break;
		}
		auto destination_it = params.find("destination");
		if (params.end() == destination_it)
		{
			command->response["message"] = "destination not provided";
			command->data_ready();
			break;
		}
		auto amount_it = params.find("amount");
		if (params.end() == amount_it)
		{
			command->response["message"] = "amount not provided";
			command->data_ready();
			break;
		}
		auto fee_it = params.find("fee");
		if (params.end() == fee_it)
		{
			command->response["message"] = "fee not provided";
			command->data_ready();
			break;
		}
		add_transaction(generate_wallet_by_private_key(std::atol(source_prk_it->second.c_str())), std::atol(destination_it->second.c_str()), std::atol(amount_it->second.c_str()), std::atol(fee_it->second.c_str()));
		command->response["_message"] = "transaction injected";
		command->data_ready();
		break;
	}
	case comm_command_type_t::add_miner:
	{
		auto& params = command->params;
		//getting wallet destination
		auto wallet_destination_it = params.find("wallet_destination");
		if (params.end() == wallet_destination_it)
		{
			command->response["message"] = "wallet_destination not provided";
			command->data_ready();
			break;
		}
		auto wallet_destination = std::atoi(wallet_destination_it->second.c_str());
		if (wallet_destination < 1)
		{
			command->response["message"] = "wallet_destination number invalid";
			command->data_ready();
			break;
		}
		//getting count
		auto count = 1;
		auto count_it = params.find("count");
		if (params.end() != count_it)
		{
			count = std::max(std::atoi(count_it->second.c_str()), count);
		}
		//creating
		auto node_id_json = nlohmann::json::array();
		for (auto i = 0; i < count; i++)
			node_id_json.push_back(add_miner(wallet_destination));
		command->response["_message"] = "miner created";
		command->response["_nodes_id"] = node_id_json;
		command->data_ready();
		break;
	}
	default:
		_ASSERT(false);
	}
}

void engine_t::process_processor_commands()
{
	transactions_t tt;
	processor_command_storage_t command;
	while (engine_processor_t::pop_command(command))
	{
		switch (command->type)
		{
		case processor_command_type_t::sign_transaction:
			{
				auto st = (processor_sign_transaction_t*)command.get();
				tt.push_back(*st->transaction);
				break;
			}
		default:
			_ASSERT(false);
		}
	}
	if (!tt.empty())
		_messages.emplace_back(new message_injected_transactions_t(-1, tt), [](auto& m) { m->release(); });
}

void engine_t::do_tick()
{
	process_comm_commands();
	process_processor_commands();
	if (_nodes.empty())
		return;
	concurrency::combinable<size_t> cc_max_pending_transactions_count([] { return 0; });
	concurrency::combinable<messages_t> cc_messages_out;
	concurrency::combinable<consensus_map_t> cc_consensus;
	auto node_operation = [&](node_storage_t& node)
	{
		auto& messages_out = cc_messages_out.local();
		node->do_tick(_ticks, _messages, messages_out);
		if (node->get_blocks_count() > 0 && !node->is_requesting_blocks())
		{
			auto block = node->get_last_block();
			auto& info = cc_consensus.local()[block.last_block_digest];
			info.block = block;
			info.node_ids.push_back(node->get_node_id());
			info.wallet_funds = node->get_wallet_funds();
		}
		if (node_type_t::miner == node->get_node_type())
		{
			auto miner = (block_chain::node_miner_t*)node.get();
			cc_max_pending_transactions_count.local() = std::max(cc_max_pending_transactions_count.local(), miner->get_pending_transactions_count());
		}
	};
	if (_parallel)
		concurrency::parallel_for_each(_nodes.begin(), _nodes.end(), node_operation);
	else
		std::for_each(_nodes.begin(), _nodes.end(), node_operation);
	_messages.clear();
	//combining messages
	cc_messages_out.combine_each([&](messages_t& cc) { for (auto& c : cc) if (_messages.end() == std::find_if(_messages.begin(), _messages.end(), [&](auto& m) { return m->is_equal(*c); })) { _messages.push_back(c); } });
	//combining consensus
	consensus_map_t current_consensus_list;
	cc_consensus.combine_each([&](const consensus_map_t& c0)
		{
			for (auto& p0 : c0)
			{
				current_consensus_list[p0.first].block = p0.second.block;
				current_consensus_list[p0.first].wallet_funds = p0.second.wallet_funds;
				std::copy(p0.second.node_ids.begin(), p0.second.node_ids.end(), std::back_inserter(current_consensus_list[p0.first].node_ids));
			}
		});
	_last_consensus_list = std::move(current_consensus_list);
	auto top_consensus_it = std::max_element(_last_consensus_list.begin(), _last_consensus_list.end(), [](const auto c0, const auto c1) { return c0.second.node_ids.size() > c1.second.node_ids.size(); });
	_last_consensus = _last_consensus_list.end() == top_consensus_it ? nullptr : &(top_consensus_it->second);
	//combining pending transactions
	_last_max_pending_transactions = 0;
	cc_max_pending_transactions_count.combine_each([&](const auto& t) { _last_max_pending_transactions = std::max(_last_max_pending_transactions, t); });
#ifdef _DEBUG
	if (_last_consensus != nullptr)
	{
		_ASSERT(_last_consensus->node_ids.end() == std::unique(_last_consensus->node_ids.begin(), _last_consensus->node_ids.end()));
	}
#endif
	//ending
	_ticks++;
}