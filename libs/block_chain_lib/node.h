#pragma once
#include "block.h"
#include <list>
#include <atomic>
#include <memory>
#include <functional>
#include <map>

namespace block_chain
{
	typedef uint32_t node_id_t;
	
	enum class message_type_t { broadcast_new_block, request_block, provide_block, injected_transactions };
	struct message_t
	{
		const node_id_t sender;
		const message_type_t message_type;
		message_t(const node_id_t s, const message_type_t mt) : sender(s), message_type(mt) {}
		virtual void release() = 0;
		virtual bool on_is_equal(const message_t& m) const = 0;
		bool is_equal(const message_t& m) const { return message_type == m.message_type && on_is_equal(m); }
		bool operator==(const message_t& m) const { return is_equal(m); }
	};
	typedef std::shared_ptr<message_t> message_storage_t;
	typedef std::list<message_storage_t> messages_t;

	struct message_broadcast_new_block_t : public message_t
	{
		block_t block;
		message_broadcast_new_block_t(const node_id_t s, const block_t& b) : message_t(s, message_type_t::broadcast_new_block), block(b) {}
		virtual void release() override { delete this; }
		virtual bool on_is_equal(const message_t& m) const override
		{
			auto& broadcast_new_block = (message_broadcast_new_block_t&)m;

			return block.block_digest == broadcast_new_block.block.block_digest;
		}
	};

	struct message_request_block_t : public message_t
	{
		number_t block_number;
		small_crypto::digest_t block_digest;
		message_request_block_t(const node_id_t s, number_t bn, small_crypto::digest_t bd) : message_t(s, message_type_t::request_block), block_number(bn), block_digest(bd) {}
		virtual void release() override { delete this; }
		virtual bool on_is_equal(const message_t& m) const override
		{
			auto& request_block = (message_request_block_t&)m;

			return block_number == request_block.block_number && block_digest == request_block.block_digest;
		}
	};

	struct message_provide_block_t : public message_t
	{
		block_t block;
		message_provide_block_t(const node_id_t s, const block_t& b) : message_t(s, message_type_t::provide_block), block(b) {}
		virtual void release() override { delete this; }
		virtual bool on_is_equal(const message_t& m) const override
		{
			auto& provide_block = (message_provide_block_t&)m;

			return block.block_digest == provide_block.block.block_digest;
		}
	};

	struct message_injected_transactions_t : public message_t
	{
		const transactions_t transactions;
		message_injected_transactions_t(const node_id_t s, const transactions_t& tt) : message_t(s, message_type_t::injected_transactions), transactions(tt) {}
		virtual void release() override { delete this; }
		virtual bool on_is_equal(const message_t& m) const override
		{
			auto& injected_transactions = (message_injected_transactions_t&)m;

			return transactions.size() == injected_transactions.transactions.size() && std::equal(transactions.begin(), transactions.end(), injected_transactions.transactions.begin(), injected_transactions.transactions.end(), [](const auto& t0, const auto& t1) { return t0.digest == t1.digest; });
		}
	};

	enum class node_type_t { base, miner };

	enum class check_new_block_return_t { ok, invalid_block, invalid_sequence, block_rejected };

	typedef std::map<small_crypto::public_key, coins_amount_t> wallet_funds_t;

	class node_t
	{
	private:
		const node_type_t _node_type;
		//id
		const uint32_t _node_id;
		//blocks
		blocks_t _blocks;
		//messages
		messages_t _messages_out;
		//wallet funds
		wallet_funds_t _wallet_funds;
		//auxilliary
		const blocks_t::const_reverse_iterator find_block_by_number(const number_t block_number) const;
		void update_wallet_funds(const block_t& block);
	protected:
		//blocks
		void add_block(const block_t& block);
		void broadcast_new_block(const block_t& new_block);
		void request_block(number_t bn, small_crypto::digest_t bd);
		void provide_block(const block_t& block);
		bool is_blocks_empty() const;
		//hooks
		virtual void on_block_provided(const block_t& new_block);
		virtual void on_message(const message_t* message);
		virtual void on_tick(const tick_t tick);
		virtual check_new_block_return_t on_check_new_block(const node_id_t sender, const block_t& new_block);
		virtual void on_broadcast_new_block(const block_t& new_block);
		virtual void on_injected_transactions(const transactions_t& tt);
		virtual void on_wallet_funds_updated(const wallet_funds_t& wallet_funds);
	public:
		node_t(const uint32_t node_id, const node_type_t node_type = node_type_t::base);
		//gets
		bool is_requesting_blocks() const;
		node_type_t get_node_type() const;
		uint32_t get_node_id() const;
		bool is_block_available(const number_t block_number) const;
		const block_t& get_block_by_number(const number_t block_number) const;
		const block_t& get_last_block() const;
		uint32_t get_blocks_count() const;
		small_crypto::digest_t get_last_block_digest() const;
		number_t get_last_block_number() const;
		const wallet_funds_t& get_wallet_funds() const;
		//operation
		virtual void release();
		virtual void do_tick(const tick_t tick, const messages_t& messages_in, messages_t& messages_out) final;
	};
}