#include "pch.h"
#include "transaction.h"
#include "config.h"
#include <numeric>
using namespace block_chain;

constexpr static auto transaction_size = sizeof(transaction_t::wallet_source) + sizeof(transaction_t::wallet_destination) + sizeof(transaction_t::amount) + sizeof(transaction_t::fee) + sizeof(transaction_t::tick);

const transaction_t block_chain::generate_transaction(const wallet_t& wallet_source, const small_crypto::public_key& wallet_destination, const coins_amount_t amount, const coins_amount_t fee, const tick_t tick)
{
	transaction_t t = { wallet_source.get_public(), wallet_destination, amount, fee, tick };
	sign_transaction(wallet_source, t);

	return t;
}

void block_chain::sign_transaction(const wallet_t& wallet_source, transaction_t& t)
{
	//Hashing
	small_crypto::hasher hasher;
	hasher.update(&t, transaction_size);
	t.digest = hasher.get_digest();
	//signing
	t.signature = small_crypto::digital_signature::do_sign(wallet_source.get_private(), t.digest);
}

bool block_chain::check_transaction(const transaction_t& t, bool check_signatures)
{
	if (0 == t.wallet_destination)
		return false;
	//Hashing
	small_crypto::hasher hasher;
	hasher.update(&t, transaction_size);
	auto digest = hasher.get_digest();
	if (digest != t.digest)
		return false;

	//verifying signature
	return !check_signatures || small_crypto::digital_signature::do_verify(t.wallet_source, digest, t.signature);
}

bool block_chain::check_transactions(const transactions_t& tt, bool check_signatures)
{
	for (auto& t : tt) if (!block_chain::check_transaction(t, check_signatures))
		return false;

	return true;
}

constexpr static auto genesis_transaction_size = sizeof(genesis_transaction_t::wallet_destination) + sizeof(genesis_transaction_t::amount);

const genesis_transaction_t block_chain::generate_genesis_transaction(const small_crypto::public_key& wallet_destination, const transactions_t& transactions)
{
	//calculating amount
	auto amount = std::accumulate(transactions.begin(), transactions.end(), coins_amount_t(config_coins_generated_each_block), [](auto& a, auto& t) { return a + t.fee; });
	//Hashing
	genesis_transaction_t gt = { wallet_destination, amount };
	small_crypto::hasher hasher;
	hasher.update(&gt, genesis_transaction_size);
	gt.digest = hasher.get_digest();

	return gt;
}

bool block_chain::check_genesis_transaction(const genesis_transaction_t& gt)
{
	if (0 == gt.wallet_destination)
		return false;
	//Hashing
	small_crypto::hasher hasher;
	hasher.update(&gt, genesis_transaction_size);
	auto digest = hasher.get_digest();

	return digest == gt.digest;
}
