#pragma once

namespace block_chain
{
	static const auto config_blocks_to_difficulty_adjust = 100;
	static const auto config_ticks_per_block = 10000;
	static const auto config_coins_generated_each_block = 1000u;
	static const auto config_first_block_number = 1u;
	static const auto config_max_transactions_per_block = 5u;
}