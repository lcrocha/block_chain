#pragma once
#include "../http_connector_lib/server.h"
#include "../http_connector_lib/json.hpp"
#include "concurrent_queue.h"
#include <thread>

namespace block_chain
{
	enum class comm_command_type_t { query_nodes, query_block_full, query_block_no_transactions, query_transaction, query_pending_transactions, query_consensus, query_processor, query_wallet_destination, add_transaction, add_miner };
	struct comm_command_t
	{
	private:
		std::mutex _m;
		std::unique_lock<std::mutex> _l;
		std::condition_variable _cv;
	public:
		comm_command_type_t type;
		std::map<std::string, std::string> params;
		nlohmann::json response;
		comm_command_t(const comm_command_type_t& type_) : type(type_), _l(_m) {}
		void data_ready() { std::lock_guard<std::mutex> l(_m); _cv.notify_all(); }
		void wait_data_ready() { _cv.wait(_l); _l.unlock(); }
	};
	typedef std::shared_ptr<comm_command_t> comm_command_storage_t;

	class engine_comm_t
	{
	private:
		//queue
		concurrent_queue<comm_command_storage_t> _commands;
		//server
		std::thread _server_thread;
		std::unique_ptr<http_connector::server_t> _server;
		//server hooks
		std::string handle_request(const comm_command_type_t command_type, http_connector::request_t& request);
		std::string on_get_request_query_nodes(http_connector::request_t& request);
		std::string on_get_request_query_transaction(http_connector::request_t& request);
		std::string on_get_request_query_pending_transactions(http_connector::request_t& request);
		std::string on_get_request_query_consensus(http_connector::request_t& request);
		std::string on_get_request_query_processor(http_connector::request_t& request);
		std::string on_get_request_query_block_full(http_connector::request_t& request);
		std::string on_get_request_query_block_no_transactions(http_connector::request_t& request);
		std::string on_get_request_query_wallet_destination(http_connector::request_t& request);
		std::string on_add_transaction(http_connector::request_t& request);
		std::string on_add_miner(http_connector::request_t& request);
	public:
		engine_comm_t(const uint32_t port_number);
		virtual ~engine_comm_t();
		//queue
		comm_command_storage_t pop_command();
		void push_command(const comm_command_storage_t& command);
	};
}
