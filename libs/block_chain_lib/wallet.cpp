#include "pch.h"
#include "wallet.h"
#include "../small_crypto_lib/digital_signature.h"
using namespace block_chain;

const wallet_t block_chain::generate_wallet_lazy()
{
	return { small_crypto::digital_signature::generate_private_key() };
}

const wallet_t block_chain::generate_wallet()
{
	return { small_crypto::digital_signature::generate_keys() };
}

const wallet_t block_chain::generate_wallet_by_private_key(const small_crypto::private_key prk)
{
	return { prk };
}

wallet_t::wallet_t(const wallet_t& wallet) : _prk(wallet._prk), _puk(wallet._puk)
{}

wallet_t::wallet_t(small_crypto::private_key x) : _prk(x), _puk()
{}

wallet_t::wallet_t(const std::tuple<small_crypto::private_key, small_crypto::public_key>& keys) : _prk(std::get<0>(keys)), _puk(std::make_shared<small_crypto::public_key>(std::get<1>(keys)))
{}

const small_crypto::private_key wallet_t::get_private() const
{
	return _prk;
}

const small_crypto::public_key wallet_t::get_public() const
{
	if (!_puk)
		_puk = std::make_shared<small_crypto::public_key>(small_crypto::digital_signature::generate_public_key(_prk));

	return *_puk;
}
