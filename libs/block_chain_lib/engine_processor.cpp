#include "pch.h"
#include "engine_processor.h"
using namespace block_chain;

void delete_processor_command(processor_command_t* t)
{
	t->release();
}

engine_processor_t::engine_processor_t() : _working(true)
{
	_thread = std::thread(std::bind(&engine_processor_t::processor_execution, this));
}

engine_processor_t::~engine_processor_t()
{
	_to_process.clear();
	_working = false;
	_thread.join();
}

size_t engine_processor_t::get_to_process_count() const
{
	return _to_process.size();
}

size_t engine_processor_t::get_processed_count() const
{
	return _processed.size();
}

void engine_processor_t::processor_execution()
{
	while (_working)
	{
		processor_command_storage_t command;
		if (!_to_process.pop(command))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			continue;
		}
		//processing commands
		switch(command->type)
		{
		case processor_command_type_t::sign_transaction:
			{
				auto st = (processor_sign_transaction_t*)command.get();
				st->transaction = std::make_unique<transaction_t>(generate_transaction(st->source, st->destination, st->amount, st->fee, st->tick));
				_processed.push(std::move(command));
				break;
			}
		default:
			_ASSERT(false);
		}
	}
}

void engine_processor_t::do_sign_transaction(const wallet_t& source, const small_crypto::public_key& destination, const coins_amount_t amount, const coins_amount_t fee, const tick_t tick)
{
	_to_process.push(new processor_sign_transaction_t(source, destination, amount, fee, tick), delete_processor_command);
}

bool engine_processor_t::pop_command(processor_command_storage_t& processor_command)
{
	return _processed.pop(processor_command);
}