#include "pch.h"
#include "engine_comm.h"
#include <sstream>
using namespace block_chain;

engine_comm_t::engine_comm_t(const uint32_t port_number)
{
	//initializing server
	_server_thread = std::thread([this, port_number]
		{
			//regestering callbacks
			http_connector::request_controller_t request_controller;
			request_controller.add_get("/query_nodes", std::bind(&engine_comm_t::on_get_request_query_nodes, this, std::placeholders::_1));
			request_controller.add_get("/query_block_full", std::bind(&engine_comm_t::on_get_request_query_block_full, this, std::placeholders::_1));
			request_controller.add_get("/query_block_no_transactions", std::bind(&engine_comm_t::on_get_request_query_block_no_transactions, this, std::placeholders::_1));
			request_controller.add_get("/query_transaction", std::bind(&engine_comm_t::on_get_request_query_transaction, this, std::placeholders::_1));
			request_controller.add_get("/add_transaction", std::bind(&engine_comm_t::on_add_transaction, this, std::placeholders::_1));
			request_controller.add_get("/add_miner", std::bind(&engine_comm_t::on_add_miner, this, std::placeholders::_1));
			request_controller.add_get("/query_pending_transactions", std::bind(&engine_comm_t::on_get_request_query_pending_transactions, this, std::placeholders::_1));
			request_controller.add_get("/query_consensus", std::bind(&engine_comm_t::on_get_request_query_consensus, this, std::placeholders::_1));
			request_controller.add_get("/query_processor", std::bind(&engine_comm_t::on_get_request_query_processor, this, std::placeholders::_1));
			request_controller.add_get("/query_wallet_destination", std::bind(&engine_comm_t::on_get_request_query_wallet_destination, this, std::placeholders::_1));
			//run server
			_server = std::make_unique<http_connector::server_t>("127.0.0.1", port_number);
			_server->start_multi_client_listener(&request_controller);
		});
}

engine_comm_t::~engine_comm_t()
{
	_server->shutdown_server = true;
	while (auto command = pop_command()) command->data_ready();
	_server_thread.join();
}

static void parse_parameters(const std::string& line, std::map<std::string, std::string>& params_map)
{
	auto start = line.find('?');
	if (std::string::npos != start)
	{
		std::stringstream params(line.substr(start + 1).c_str());
		std::string param;
		while (std::getline(params, param, '&'))
		{
			auto equal = param.find('=');
			if (std::string::npos == equal)
				continue;
			auto name = param.substr(0, equal);
			if (name.empty())
				continue;
			auto value = param.substr(equal + 1, param.size() - equal - 1);
			if (value.empty())
				continue;
			params_map[name] = value;
		}
	}
}

std::string engine_comm_t::handle_request(const comm_command_type_t command_type, http_connector::request_t& request)
{
	request.response.headers["Content-Type"] = "text/plain";
	auto command = std::make_shared<comm_command_t>(command_type);
	//parsing parameters
	parse_parameters(request.end_point, command->params);
	//adding params in input request
	command->response["_params"] = command->params;
	//releasing command
	push_command(command);
	std::string messageToSend = command->response.dump(4);
	request.response.headers["Access-Control-Allow-Origin"] = "*";
	request.response.headers["Content-Length"] = std::to_string(messageToSend.size());
	request.response.add_body_text(messageToSend);

	return request.response.to_string();
}

comm_command_storage_t engine_comm_t::pop_command()
{
	comm_command_storage_t command;
	_commands.pop(command);

	return command;
}

void engine_comm_t::push_command(const comm_command_storage_t& command)
{
	_commands.push(command);
	command->wait_data_ready();
}

//http://127.0.0.1:1248/query_nodes
std::string engine_comm_t::on_get_request_query_nodes(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_nodes, request);
}

//http://127.0.0.1:1248/query_block_full?block_number=&node_id=
std::string engine_comm_t::on_get_request_query_block_full(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_block_full, request);
}

//http://127.0.0.1:1248/query_block_no_transactions?block_number=&node_id=
std::string engine_comm_t::on_get_request_query_block_no_transactions(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_block_no_transactions, request);
}

//http://127.0.0.1:1248/query_transaction?block_number=&node_id=&transaction_number=
std::string engine_comm_t::on_get_request_query_transaction(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_transaction, request);
}

//http://127.0.0.1:1248/query_pending_transactions?node_id=
std::string engine_comm_t::on_get_request_query_pending_transactions(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_pending_transactions, request);
}

//http://127.0.0.1:1248/query_consensus
std::string engine_comm_t::on_get_request_query_consensus(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_consensus, request);
}

//http://127.0.0.1:1248/query_processor
std::string engine_comm_t::on_get_request_query_processor(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_processor, request);
}

//http://127.0.0.1:1248/query_wallet_destination?private_key=1
std::string engine_comm_t::on_get_request_query_wallet_destination(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::query_wallet_destination, request);
}

//http://127.0.0.1:1248/add_transaction?source_privatekey=1&destination=2&amount=34500&fee=345
std::string engine_comm_t::on_add_transaction(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::add_transaction, request);
}

//http://127.0.0.1:1248/add_miner?wallet_destination=1&count=1
std::string engine_comm_t::on_add_miner(http_connector::request_t& request)
{
	return handle_request(comm_command_type_t::add_miner, request);
}