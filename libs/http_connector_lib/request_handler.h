#pragma once
#include <unordered_map>
#include <map>
#include <vector>
#include <functional>

namespace http_connector
{
	enum http_status_code_t
	{
		OK = 200,
		BAD_REQUEST = 400,
		NOT_FOUND = 404,
		METHOD_NOT_ALLOWED = 405,
		INTERNAL_SERVER_ERROR = 500,
		NOT_IMPLEMENTED = 501
	};

	struct status_code_t : public std::map<std::string, http_status_code_t>
	{
		status_code_t();
		virtual ~status_code_t();
	};

	struct status_code_string_t : public std::map<http_status_code_t, std::string>
	{
		status_code_string_t();
		virtual ~status_code_string_t();
	};

	struct response_t
	{
		std::vector<std::string> body;
		http_status_code_t status_code = OK;
		std::map<std::string, std::string> headers;

		void add_body_text(std::string text);
		std::string to_string();
	};

	struct request_t
	{
		std::string verb;
		std::string end_point;

		std::map<std::string, std::string> headers;
		std::string body;

		std::string payload; //request full text

		response_t response;

		request_t(std::string sPayload);
	};

	class request_handler_i
	{
	public:
		virtual std::string handle_request(std::string payload) = 0;
	};

	class request_controller_t : public request_handler_i
	{
	private:

		/// <summary>
		/// Endpoints for GET verb
		/// </summary>
		std::unordered_map<std::string, std::function<std::string(request_t)>> end_points_get;

		/// <summary>
		/// Endpoints for POST verb
		/// </summary>
		std::unordered_map<std::string, std::function<std::string(request_t)>> end_points_post;

	public:

		std::string handle_request(std::string payload);

		/// <summary>
		/// Add a new GET endpoint
		/// </summary>
		/// <param name="endPoint">Endpoint eg. /person</param>
		/// <param name="handler">function to handle the request and response</param>
		void add_get(std::string end_point, std::function<std::string(request_t&)> handler);

		/// <summary>
		/// Add a new POST endpoint
		/// </summary>
		/// <param name="endPoint">Endpoint eg. /person</param>
		/// <param name="handler">function to handle the request and response</param>
		void add_post(std::string end_point, std::function<std::string(request_t&)> handler);

		virtual ~request_controller_t();
	};
}