#include "request_handler.h"
#include <sstream>
#include <algorithm>
using namespace http_connector;

status_code_t::status_code_t()
{
	this->operator[]("200") = OK;
	this->operator[]("400") = BAD_REQUEST;
	this->operator[]("404") = NOT_FOUND;
	this->operator[]("405") = METHOD_NOT_ALLOWED;
	this->operator[]("500") = INTERNAL_SERVER_ERROR;
	this->operator[]("501") = NOT_IMPLEMENTED;
}

status_code_t::~status_code_t()
{}

status_code_string_t::status_code_string_t()
{
	this->operator[](OK) = "OK";
	this->operator[](BAD_REQUEST) = "Bad request_t";
	this->operator[](NOT_FOUND) = "Not Found";
	this->operator[](METHOD_NOT_ALLOWED) = "405";
	this->operator[](INTERNAL_SERVER_ERROR) = "500";
	this->operator[](NOT_IMPLEMENTED) = "501";
}

status_code_string_t::~status_code_string_t()
{}

void response_t::add_body_text(std::string text)
{
	body.push_back(text);
}

std::string response_t::to_string()
{
	std::string final_response;
	std::ostringstream ss;
	ss << "HTTP/1.1 " << status_code << " " << status_code_string_t()[status_code] << "\r\n";

	for (const auto key_value : headers)
	{
		ss << key_value.first << ": " << key_value.second << "\r\n";
	}

	std::string body_buffer;
	for (const std::string& s : body)
	{
		body_buffer.append(s);
	}

	if (!body.empty())
		ss << "\r\n" << body_buffer << "\r\n";

	final_response.append(ss.str());

	return final_response;
}

request_t::request_t(std::string sPayload)
{
	payload = sPayload;
	if (payload.empty())
	{
		response.status_code = BAD_REQUEST;
		std::ostringstream ss;
		ss << "HTTP/1.1 " << std::to_string(response.status_code) << " " << status_code_string_t()[response.status_code];
		response.add_body_text(ss.str());
	}
	else
	{
		response.status_code = OK;

		bool first_line_parsed = false;
		bool headers_parsed = false;

		auto c_payload = const_cast<char*>(payload.c_str());
		char* next_token = nullptr;
		char* token = strtok_s(c_payload, "\r", &next_token);

		while (token != nullptr)
		{
			//body delimiter
			if (strlen(token) == 1 && token[0] == '\n')
			{
				headers_parsed = true;
				token = strtok_s(nullptr, "\r", &next_token);
				continue;
			}

			//parse first line
			if (!first_line_parsed)
			{
				char* next_space = nullptr;
				char* token_white_space = strtok_s(token, " ", &next_space);
				while (token_white_space != nullptr)
				{
					std::string value = std::string(token_white_space);
					if (value.find('\n') != std::string::npos)
						value.erase(value.find('\n'), 1);
					if (verb.empty())
					{
						verb = value;
					}
					else if (end_point.empty())
					{
						end_point = value;
						first_line_parsed = true;
						break;
					}
					token_white_space = strtok_s(nullptr, " ", &next_space);
				}
				first_line_parsed = true;
			}
			//headers
			else if (!headers_parsed && std::string(token).find(":") != std::string::npos)
			{
				char* next_delimiter = nullptr;
				char* delimiter = strtok_s(token, ":", &next_delimiter);
				std::string key = delimiter;
				if (key.find('\n') != std::string::npos)
					key.erase(key.find('\n'), 1);
				delimiter = strtok_s(nullptr, ":", &next_delimiter);
				delimiter = delimiter == nullptr ? (char*)"\0" : delimiter;
				headers.emplace(std::pair< std::string, std::string>(key, delimiter));
			}
			//body
			else if (headers_parsed)
			{
				if (token != nullptr)
				{
					std::string value = std::string(token);
					if (value.find('\n') != std::string::npos)
						value.erase(value.find('\n'), 1);
					if (!value.empty())
						body.append(value);
				}
			}
			token = strtok_s(nullptr, "\r", &next_token);
		}
	}
}

std::string request_controller_t::handle_request(std::string payload)
{
	request_t request(payload);

	if (request.verb == "GET")
	{
		auto findEndPoint = std::find_if(end_points_get.begin(), end_points_get.end(),
			[&request](std::pair<std::string, std::function<std::string(request_t)>> const& endPoint)
			{
				return std::string::npos != request.end_point.find(endPoint.first);
			});
		if (findEndPoint != end_points_get.end())
			return findEndPoint->second(request);
	}
	else if (request.verb == "POST")
	{
		auto findEndPoint = std::find_if(end_points_post.begin(), end_points_post.end(),
			[&request](std::pair<std::string, std::function<std::string(request_t)>> const& endPoint)
			{
				return std::string::npos != request.end_point.find(endPoint.first);
			});
		if (findEndPoint != end_points_post.end())
			return findEndPoint->second(request);
	}

	request.response.status_code = NOT_FOUND;
	request.response.headers["Content-Length"] = "0";
	return request.response.to_string();
}

void request_controller_t::add_get(std::string endPoint, std::function<std::string(request_t&)> handler)
{
	end_points_get.emplace(std::pair<std::string, std::function<std::string(request_t&)>>(endPoint, handler));
}

void request_controller_t::add_post(std::string endPoint, std::function<std::string(request_t&)> handler)
{
	end_points_post.emplace(std::pair<std::string, std::function<std::string(request_t&)>>(endPoint, handler));
}

request_controller_t::~request_controller_t()
{
	end_points_get.clear();
	end_points_post.clear();
}