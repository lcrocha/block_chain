# block chain - a simple block chain implementation
This simple implementation of a digital cryptocurrency system based on a simple block chain. For the consensus mechanism it is used a **proof of work**. For the sake of simplicity the two cryptographic algorithms needed to implement the block chain and the cryptocurrency itself uses a simple 32bits hash function and a 16bits key sizes for the digital signature algorithm.

Thanks to [Julio Cesar Schincariol Filho](mailto:jcschincariolfh@gmail.com) who implemented the lightweight http connector server.

Keywords: Visual Studio, C/C++, block chain, cryptocurrency, hash, dsa, HTML, CSS, REST, HTTP request, unit tests

## File structure
The structure bellow for the projects using Visual Studio 2019

	+ block_chain_app
	|	|
	|	+ block_chain_app - A command prompt app
	|
	+ dashboard - a HTML/css client to interact with the block_chain_app
	|
	+ libs
	|	|
	|	+ block_chain_lib - The lib with the block chain implementation whereas the blocks, transactions, nodes, etc are defined
	|	|
	|	+ http_connect_lib - The lib that implementes the http rest server to receive the requests
	|	|
	|	+ small_crypto_lib - The lib with the key cryptography components such the simple hasher and simple digital signature algorithms based in modular arithmethics
	|
	+ unit_tests - unit tests based on visual studio built in suite tests


## To compile
1) on the root of block_chain folder, open the solution with Visual Studio 2019

## To run
Sintax:

    block_chain.exe [http-listen-port-number] [miners-count] [initial-wallets]
        - http-listen-port-number: port number to be open used by the http server to listen the requests (default: 1248, greater than zero)
        - miners-count: how many miners to start with (default: 400, min: 1)
        - initial-wallets: how many wallets should be created and to be associated with the miners (default: 10, min: 1)


## Requests
1) Query nodes: http://127.0.0.1:1248/query_nodes

        Description: Return the status of the current nodes
        Parameters: none
        Return: json format

2) Query full block: http://127.0.0.1:1248/query_block_full?block_number=&node_id=

        Description: Returns the info of the current block with all transactions
        Parameters:
            block_number:   number of the block (empty for the last one)
            node_id:        node id (empty for node 0)
        Return: json format

3) Query block with no transactions: http://127.0.0.1:1248/query_block_no_transactions_list?block_number=&node_id=

        Description: Returns the info of the current block with transactions count only
        Parameters:
            block_number:   number of the block (empty for the last one)
            node_id:        node id (empty for node 0)
        Return: json format

4) Query a transactions from a block: http://127.0.0.1:1248/query_transaction?block_number=&node_id=&transaction_number=

        Description: Returns specific transaction number from a block
        Parameters:
            block_number:               number of the block (empty for the last one)
            node_id:                    node id (empty for node 0)
            transaction_number:         number of the transaction (empty for first transaction, zero based index)
        Return: json format

5) Add a transaction: http://127.0.0.1:1248/add_transaction?source_privatekey=1&destination=2&amount=34500&fee=345

        Description: Broadcast a transaction. All parameters are required.
        Parameters:
            source_privatekey:      the source private key to sign the transaction
            destination:            the wallet destination for the coins
            amount:                 amount of coins to be transfered
            fee:                    fee for the miner
        Return: json format

## Keywords

### Generator numbers
Numbers selected and shared to be used by the cryptography algorithms throughout the application.

### Private Key
A random number inside the domain of the **generator numbers** of the algorithm used to **sign transactions**.

### Public key
A number generated from the selected **private key**.

### Signing transaction, DSA, Digital Signature Algorithm
An arithmetic operation to find a number, hence called **signature**, based on the **private key** where using the **public key** it is possible to check if the transaction is valid or not. Important to note that it is one way function, it means you will not be able, or at least not so easy, to find a **private key** used to sign a transaction based only in the **signature** and the **public key**.

### Wallet
A struct with a **private key** and a **public key**

### Tick
A time unit used in this application. It represents one execution to all nodes.

### Amount, Fee
A coin quantity. It is an integer number, cannot be fraccionated.

### Transaction
Transaction is a register of the **wallet source**, **wallet destination**, **amount**, **fee**, **tick** and a **signature**. This **signature** is done via **dsa** using the **private key** from the **wallet_source**.

### Genesis transaction
A special **transaction** described on the block where new **coins** are created and all **fee** are collect to reward the **miner** who found to correct **nonce**.

### Digest
A hash number of the current data submitted.

### Nonce
Random number added to each **block** changed by the miners to **mine a block**

### Mine a block
Operation performed by the miners where the **nonce** is changed to find a **digest** lower or equal to the difficulty.

### Difficulty
It is a number used by the miners to mine a block. The difficulty is adjusted based on the system constant to ballance the frequency of the **mining blocks**. For instance, the system is set to have one **block** each 1000 **ticks**, and after 100 blocks, the difficulty will be adjusted accordingly. If the 100 blocks were **mined** before 100 * 1000 ticks, the difficulty number will be **decreased**, it means that it will be harder to **mine a block**, and in other hand, if the 100 blocks were mined after 100 * 1000 tickes, the difficulty number will be **increased**, it means that it will be easier to **mine a block**.

### Block
Block is a collection of **transactions**, the **digest** from the last block, the current **difficulty**, the current **block number**, the current **tick**, the **nonce**, the **genesis transaction**, and the block **digest**.

### Nodes
They are responsible to keep the **block chain** in memory.

### Miner
Special type of **node** that collect all pending **transactions**, with the **digest** of the last block, the **genesis transaction** and keep changing the **nonce** to find a new **digest** that is lower or equal than the current **difficulty**.