#include "../libs/block_chain_lib/engine.h"
#include <conio.h>
#include <iostream>
#include <iomanip>

#define RESET_ON_BLOCK 0

void auto_injector_test(int argc, char* argv[])
{
	//parameters
	auto port_number = 1248;
	auto miners_count = 400;
	auto wallets_count = 10;
	//listen port parameter
	if (argc > 1)
	{
		auto new_number = std::atoi(argv[1]);
		port_number = new_number > 0 ? new_number : port_number;
		//miners count
		if (argc > 2)
		{
			auto new_miners_count = std::atoi(argv[2]);
			miners_count = new_miners_count > 0 ? new_miners_count : miners_count;
			//wallets count
			if (argc > 3)
			{
				auto new_wallets_count = std::atoi(argv[3]);
				wallets_count = new_wallets_count > 0 ? new_wallets_count : wallets_count;
			}
		}
	}
	//setting engine
	std::vector<block_chain::wallet_t> wallets;
	//generating wallets
	std::cout << "block chain app is starting..." << std::endl;
	for (auto i = 0; i < wallets_count; i++)
	{
		wallets.emplace_back(block_chain::generate_wallet_lazy());
		std::cout << "\rgenerating wallets: " << i + 1 << "/" << wallets_count;
	}
	std::cout << std::endl;
	//generating miners
	std::unique_ptr<block_chain::engine_t> engine;
	auto start_engine = [&]
	{
		engine = std::make_unique< block_chain::engine_t>(port_number);
		for (auto i = 0; i < miners_count; i++)
		{
			std::cout << "\rgenerating miners: " << i + 1 << "/" << miners_count;
			engine->add_miner(wallets[i % wallets.size()].get_public());
		}
	};
	//transaction injector
	auto inject_transaction = [&]()
	{
		auto size = number_t(wallets.size() - 1);
		auto source = small_crypto::random(0u, size);
		auto destination = small_crypto::random(0u, size);
		while (source == destination)
			destination = small_crypto::random(0, size);
		auto amount = small_crypto::random(1, uint32_t(1E5));
		auto& consensus_list = engine->get_last_consensus_list();
		if (!consensus_list.empty())
		{
			auto& first_consensus = consensus_list.begin()->second;
			auto source_funds_it = first_consensus.wallet_funds.find(wallets[source].get_public());
			if (first_consensus.wallet_funds.end() != source_funds_it && source_funds_it->second > 1)
				amount = small_crypto::random(1, 2 * source_funds_it->second);
		}
		engine->add_transaction(wallets[source], wallets[destination].get_public(), amount, amount / 100);
	};
	//frame
	auto inject_transactions = true;
	auto loop = true;
	auto last_legend_tick = 0u;
	while (loop)
	{
		if (!engine)
			start_engine();
		if (!engine->is_parallel())
			std::this_thread::sleep_for(std::chrono::microseconds(100));
		else
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		//keyboard
		if (::_kbhit())
		{
			auto c = _getch();
			switch (c)
			{
			case 27:
				loop = false;
				break;
			case 32:
				inject_transactions = !inject_transactions;
				break;
			case 13:
				engine->toggle_parallel();
				break;
			}
		}
		//process
		auto now = ::GetTickCount();
		const auto nodes_time = 1000 / 60;
		while (::GetTickCount() - now < nodes_time)
		{
			engine->do_tick();
		}
		//inject transactions
		if (inject_transactions)
		{
			auto pending = engine->get_to_process_count() + engine->get_max_pending_transactions();
			//if (0 == small_crypto::random(0, uint32_t(chance)))
			if (pending < 10)
			{
				inject_transaction();
			}
		}
		//legend
		auto elapsed = now - last_legend_tick;
		if (elapsed < 1000)
			continue;
		last_legend_tick = now;
		std::system("cls");
		std::cout << "block chain app / listen port: " << port_number << std::endl;
		//nodes
		std::cout << "nodes: " << engine->get_nodes_count() << std::endl;
		//ticks
		auto ticks = engine->get_ticks_count();
		std::cout << "tick: " << ticks << std::endl;
		static size_t last_tick = 0u;
		std::cout << "ticks per sec: " << (1000 * (ticks - last_tick)) / elapsed << std::endl;
		last_tick = ticks;
		//preparing transactions
		std::cout << std::endl;
		std::cout << "preparing transactions" << std::endl;
		std::cout << "to process: " << engine->get_to_process_count() << std::endl;
		//transactions
		std::cout << std::endl;
		std::cout << "pending transactions: " << engine->get_max_pending_transactions() << std::endl;
		//blocks
		std::cout << std::endl;
		auto& consensus_list = engine->get_last_consensus_list();
		std::cout << "consensus list: " << consensus_list.size() << std::endl;
		for (auto info : consensus_list)
		{
			std::cout
				<< "\tdigest: " << std::setw(4) << info.first
				<< "\tdifficulty: " << std::setw(4) << info.second.block.difficulty
				<< "\tblock number: " << std::dec << std::setw(6) << info.second.block.block_number
				<< "\ttransactions count: " << info.second.block.transactions.size()
				<< "\tnodes count: " << info.second.node_ids.size() << std::endl;
#if (RESET_ON_BLOCK > 0)
			if (info.second.block.block_number >= RESET_ON_BLOCK)
				reset = true;
#endif
		}
		//wallets
		auto top_consensus = engine->get_last_consensus();
		if (nullptr != top_consensus)
		{
			auto total_coins = 0u;
			std::cout << std::endl;
			std::cout << "wallet funds: " << top_consensus->wallet_funds.size() << std::endl;
			for (auto wallet_fund : top_consensus->wallet_funds)
			{
				total_coins += wallet_fund.second;
				std::cout
					<< "\twallet: " << std::setw(8) << wallet_fund.first
					<< "\tamount: " << std::setw(8) << wallet_fund.second
					<< std::endl;
			}
			std::cout << "total coins: " << total_coins << std::endl;
		}
		//commands
		std::cout << std::endl;
		std::cout
			<< "<ESC> - End application  "
			<< "\t<SPACE> - Toggle inject transactions[" << (inject_transactions ? "ON] " : "OFF]")
			<< "\t<ENTER> - Toggle use all cores[" << (engine->is_parallel() ? "ON]" : "OFF]");

#if (RESET_ON_BLOCK > 0)
		if (reset)
		{
			engine.reset();
			last_tick = 0u;
		}
#endif
	}
}

void standard_test(int argc, char* argv[])
{
	//parameters
	auto port_number = 1248;
	//listen port parameter
	if (argc > 1)
	{
		auto new_number = std::atoi(argv[1]);
		port_number = new_number > 0 ? new_number : port_number;
	}
	//setting engine
	std::unique_ptr<block_chain::engine_t> engine;
	auto start_engine = [&]
	{
		engine = std::make_unique< block_chain::engine_t>(port_number);
	};
	//frame
	auto loop = true;
	auto last_legend_tick = 0u;
	while (loop)
	{
		auto reset = false;
		if (!engine)
			start_engine();
		if (!engine->is_parallel())
			std::this_thread::sleep_for(std::chrono::microseconds(100));
		else
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		//keyboard
		if (::_kbhit())
		{
			auto c = _getch();
			switch (c)
			{
			case 27:
				loop = false;
				break;
			case 13:
				engine->toggle_parallel();
				break;
			case 'r':
			case 'R':
				reset = true;
				break;
			}
		}
		//process
		auto now = ::GetTickCount();
		const auto nodes_time = 1000 / 60;
		while (::GetTickCount() - now < nodes_time)
		{
			engine->do_tick();
		}
		//legend
		auto elapsed = now - last_legend_tick;
		static size_t last_tick = 0u;
		if (elapsed >= 1000)
		{
			last_legend_tick = now;
			std::system("cls");
			std::cout << "block chain app / listen port: " << port_number << std::endl;
			//nodes
			std::cout << "nodes: " << engine->get_nodes_count() << std::endl;
			//ticks
			auto ticks = engine->get_ticks_count();
			std::cout << "tick: " << ticks << std::endl;
			std::cout << "ticks per sec: " << (1000 * (ticks - last_tick)) / elapsed << std::endl;
			last_tick = ticks;
			//preparing transactions
			std::cout << std::endl;
			std::cout << "preparing transactions" << std::endl;
			std::cout << "to process: " << engine->get_to_process_count() << std::endl;
			//transactions
			std::cout << std::endl;
			std::cout << "pending transactions: " << engine->get_max_pending_transactions() << std::endl;
			//blocks
			std::cout << std::endl;
			auto& consensus_list = engine->get_last_consensus_list();
			std::cout << "consensus list: " << consensus_list.size() << std::endl;
			for (auto info : consensus_list)
			{
				std::cout
					<< "\tdigest: " << std::setw(4) << info.first
					<< "\tdifficulty: " << std::setw(4) << info.second.block.difficulty
					<< "\tblock number: " << std::dec << std::setw(6) << info.second.block.block_number
					<< "\ttransactions count: " << info.second.block.transactions.size()
					<< "\tnodes count: " << info.second.node_ids.size() << std::endl;
#if (RESET_ON_BLOCK > 0)
				if (info.second.block.block_number >= RESET_ON_BLOCK)
					reset = true;
#endif
			}
			//wallets
			auto top_consensus = engine->get_last_consensus();
			if (nullptr != top_consensus)
			{
				auto total_coins = 0u;
				std::cout << std::endl;
				std::cout << "wallet funds: " << top_consensus->wallet_funds.size() << std::endl;
				for (auto wallet_fund : top_consensus->wallet_funds)
				{
					total_coins += wallet_fund.second;
					std::cout
						<< "\twallet: " << std::setw(8) << wallet_fund.first
						<< "\tamount: " << std::setw(8) << wallet_fund.second
						<< std::endl;
				}
				std::cout << "total coins: " << total_coins << std::endl;
			}
			//commands
			std::cout << std::endl;
			std::cout
				<< "<ESC> - End application  "
				<< "\t<ENTER> - Toggle use all cores[" << (engine->is_parallel() ? "ON]" : "OFF]");
		}
		if (reset)
		{
			engine.reset();
			last_tick = 0u;
		}
	}
}

int main(int argc, char* argv[])
{
	//auto_injector_test(argc, argv);
	standard_test(argc, argv);
}