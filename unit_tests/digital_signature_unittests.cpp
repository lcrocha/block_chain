#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/small_crypto_lib/digital_signature.h"
#include <limits>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace small_crypto_unittests
{
	TEST_CLASS(digital_signature_unittests)
	{
	public:
		TEST_METHOD(signature_simple_max)
		{
			small_crypto::digital_signature dsa;
			small_crypto::private_key prk;
			small_crypto::public_key puk;
			std::tie(prk, puk) = dsa.generate_keys();
			//signing
			number_t hash = std::numeric_limits<number_t>::max();
			auto sig = dsa.do_sign(prk, hash);
			//verifying
			Assert::IsTrue(dsa.do_verify(puk, hash, sig));
			((number_t&)sig.s)++;
			Assert::IsFalse(dsa.do_verify(puk, hash, sig));
			((number_t&)sig.s)--;
			hash--;
			Assert::IsFalse(dsa.do_verify(puk, hash, sig));
		}

		TEST_METHOD(signature_simple_min)
		{
			small_crypto::digital_signature dsa;
			small_crypto::private_key prk;
			small_crypto::public_key puk;
			std::tie(prk, puk) = dsa.generate_keys();
			//signing
			number_t hash = std::numeric_limits<number_t>::min();
			auto sig = dsa.do_sign(prk, hash);
			//verifying
			Assert::IsTrue(dsa.do_verify(puk, hash, sig));
			((number_t&)sig.s)++;
			Assert::IsFalse(dsa.do_verify(puk, hash, sig));
			((number_t&)sig.s)--;
			hash--;
			Assert::IsFalse(dsa.do_verify(puk, hash, sig));
		}
	};
}
