#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/block_chain_lib/wallet.h"
#include "../libs/block_chain_lib/transaction.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace block_chain_unittests
{
	TEST_CLASS(transaction_unittests)
	{
	public:
		TEST_METHOD(generate_transaction_simple)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			Assert::IsTrue(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_digest)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			t0.digest++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_signature_r)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			((number_t&)t0.signature.r)++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_signature_s)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			((number_t&)t0.signature.s)++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_amount)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			t0.amount++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_fee)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			t0.fee++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}

		TEST_METHOD(generate_transaction_bad_wallet_destination)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_transaction(w0, 0xff, 10000u, 1u, 0);
			t0.wallet_destination++;
			Assert::IsFalse(block_chain::check_transaction(t0));
		}
	};
}