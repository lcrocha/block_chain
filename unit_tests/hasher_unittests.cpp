#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/small_crypto_lib/hasher.h"
#include <vector>
#include <numeric>
#include <ppl.h>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace small_crypto_unittests
{
	TEST_CLASS(hasher_unittests)
	{
	public:
		TEST_METHOD(hash_simple)
		{
			std::vector<uint8_t> buff(100);
			std::iota(buff.begin(), buff.end(), 0);
			small_crypto::hasher h;
			h.update(buff.data(), uint32_t(buff.size()));
			auto d = h.get_digest();
			Assert::IsTrue(0 != d);
		}

		TEST_METHOD(hash_all_bytes)
		{
			constexpr auto max = 0xffff;// std::numeric_limits<Number>::max();
			concurrency::parallel_for<uint32_t>(0, max, [](auto n)
				{
					small_crypto::hasher h;
					h.update(n);
					auto d = h.get_digest();
					Assert::IsTrue(n != d);
				});
		}
	};
}
