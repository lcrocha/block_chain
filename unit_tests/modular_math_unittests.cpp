#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/small_crypto_lib/modular_math.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace modular_math_unittests
{
	TEST_CLASS(modular_math_unittests)
	{
	private:
		bool is_prime(const number_t n)
		{
			if (n < 2)
				return false;
			for (number_t i = 2; i <= n / 2; i++) if (0 == n % i)
				return false;
		
			return true;
		}

		number_t pow(const number_t base, const number_t exp, const number_t mod)
		{
			_ASSERT(base < mod&& exp < mod&& mod > 1);
			number_t c = base > 0 ? 1 : 0;
			for (number_t e = 0; e < exp; e++)
				c = (c * base) % mod;

			return c;
		}

	public:
		TEST_METHOD(modular_inverse_simple)
		{
			const number_t mod = 499;
			for (number_t n = 1; n < mod; n++)
			{
				auto ni = modular_math::inverse(n, mod);
				Assert::IsTrue(1 == ((n * ni) % mod));
				Assert::IsTrue(1 == modular_math::mul(n, ni, mod));
			}
		}

		TEST_METHOD(modular_primes)
		{
			for (auto i = 0; i < 0xffff; i++)
			{
				Assert::IsTrue(modular_math::is_prime(i) == is_prime(i));
			}
		}

		TEST_METHOD(modular_pow)
		{
			auto mod = 0xff;
			for (auto a = 0; a < mod; a++) for (auto b = 0; b < mod; b++)
			{
				auto r0 = modular_math::pow(a, b, mod);
				auto r1 = pow(a, b, mod);
				Assert::IsTrue(r0 == r1);
			}
		}

		TEST_METHOD(modular_mul)
		{
			auto mod = 0xff;
			for (auto a = 0; a < mod; a++) for (auto b = 0; b < mod; b++)
			{
				auto r0 = modular_math::mul(a, b, mod);
				auto r1 = (a * b) % mod;
				Assert::IsTrue(r0 == r1);
			}
		}

		TEST_METHOD(modular_add)
		{
			auto mod = 0xfff;
			for (auto a = 0; a < mod; a++) for (auto b = 0; b < mod; b++)
			{
				auto r0 = modular_math::add(a, b, mod);
				auto r1 = (a + b) % mod;
				Assert::IsTrue(r0 == r1);
			}
		}
	};
}
