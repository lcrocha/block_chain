#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/block_chain_lib/wallet.h"
#include "../libs/block_chain_lib/transaction.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace block_chain_unittests
{
	TEST_CLASS(genesis_transaction_unittests)
	{
	public:
		TEST_METHOD(generate_genesis_transaction_simple)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_genesis_transaction(w0.get_public(), {});
			Assert::IsTrue(block_chain::check_genesis_transaction(t0));
		}

		TEST_METHOD(generate_genesis_transaction_bad_digest)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_genesis_transaction(w0.get_public(), {});
			t0.digest++;
			Assert::IsFalse(block_chain::check_genesis_transaction(t0));
		}

		TEST_METHOD(generate_genesis_transaction_bad_amount)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_genesis_transaction(w0.get_public(), {});
			t0.amount++;
			Assert::IsFalse(block_chain::check_genesis_transaction(t0));
		}

		TEST_METHOD(generate_genesis_transaction_bad_wallet_destination)
		{
			auto w0 = block_chain::generate_wallet();
			auto t0 = block_chain::generate_genesis_transaction(w0.get_public(), {});
			t0.wallet_destination++;
			Assert::IsFalse(block_chain::check_genesis_transaction(t0));
		}
	};
}