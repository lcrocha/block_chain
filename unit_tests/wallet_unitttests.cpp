#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/block_chain_lib/wallet.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace block_chain_unittests
{
	TEST_CLASS(wallet_unittests)
	{
	public:
		TEST_METHOD(generate_wallet)
		{
			auto w0 = block_chain::generate_wallet();
			auto w1 = block_chain::generate_wallet();
			Assert::IsTrue(w0.get_public() != w1.get_public());
			Assert::IsTrue(w0.get_private() != w1.get_private());
		}
	};
}