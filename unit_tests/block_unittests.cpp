#include "pch.h"
#include "CppUnitTest.h"
#include "../libs/block_chain_lib/block.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace block_chain_unittests
{
	TEST_CLASS(block_unittests)
	{
	private:
		const block_chain::difficulty_t max_difficulty = std::numeric_limits<block_chain::difficulty_t>::max();

		std::vector<block_chain::transaction_t> generate_transactions(const unsigned quantity)
		{
			std::vector<block_chain::transaction_t> tt;
			for (auto i = 0u; i < quantity; i++)
			{
				auto wallet_source = block_chain::generate_wallet();
				auto wallet_destination = block_chain::generate_wallet();
				auto amount = 1000 * small_crypto::random(1, 10000);
				Assert::IsTrue(amount > 0.0f);
				auto fee = amount / 100u;
				Assert::IsTrue(fee > 0.0f);
				tt.emplace_back(block_chain::generate_transaction(wallet_source, wallet_destination.get_public(), amount, fee, 0));
			}

			return tt;
		}
	public:
		TEST_METHOD(block_empty)
		{
			for (auto i = 0; i < 5; i++)
			{
				auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, i, {});
				Assert::IsTrue(0 != block.block_digest);
				auto r = block_chain::check_block(block);
				Assert::IsTrue(r);
			}
		}

		TEST_METHOD(block_bad_formed_last_block_digest)
		{
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, {});
			Assert::IsTrue(0 != block.block_digest);
			block.last_block_digest++;
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(block_bad_formed_difficulty)
		{
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, {});
			Assert::IsTrue(0 != block.block_digest);
			block.difficulty++;
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(block_bad_formed_block_number)
		{
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, {});
			Assert::IsTrue(0 != block.block_digest);
			block.block_number++;
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(block_bad_formed_nonce)
		{
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, {});
			Assert::IsTrue(0 != block.block_digest);
			block.nonce++;
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(block_bad_formed_wallet)
		{
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, {});
			Assert::IsTrue(0 != block.block_digest);
			block.genesis_transaction.wallet_destination++;
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(block_well_formed_transactions)
		{
			//setting transactions
			auto tt = generate_transactions(5);
			//generating block
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, tt);
			Assert::IsTrue(0 != block.block_digest);
			auto r = block_chain::check_block(block);
			Assert::IsTrue(r);
		}

		TEST_METHOD(block_bad_formed_transactions)
		{
			//setting transactions
			auto tt = generate_transactions(5);
			((block_chain::transaction_t&)tt.back()).digest++;
			//generating block
			auto block = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, tt);
			Assert::IsTrue(0 != block.block_digest);
			auto r = block_chain::check_block(block);
			Assert::IsFalse(r);
		}

		TEST_METHOD(chain_block_ok)
		{
			auto b0 = block_chain::generate_genesis_block(0xff, max_difficulty, 0, 0x0, generate_transactions(5));
			auto b1 = block_chain::generate_block(0xff, b0, max_difficulty, 1, 0x0, generate_transactions(5));
			Assert::IsTrue(block_chain::check_block(b0, b1));
		}

		//TEST_METHOD(chain_block_mining)
		//{
		//	auto b0 = block_chain::generate_genesis_block(0x0, 0x0, max_difficulty, 0x0, generate_transactions(5));
		//	block_chain::block_t b1 = {};
		//	auto tt = generate_transactions(5);
		//	constexpr auto max = std::numeric_limits<small_crypto::digest_t>::max();
		//	for (auto i = 0; i < 31; i++)
		//	{
		//		auto attempts = 0u;
		//		auto difficulty = max >> i;
		//		do
		//		{
		//			b1 = block_chain::generate_genesis_block(b0, difficulty, small_crypto::random(0x0, 0xffffffff), tt);
		//			attempts++;
		//		} while (b1.difficulty <= b1.block_digest);
		//		Assert::IsTrue(block_chain::check_block(b0, b1));
		//		std::ostringstream oss;
		//		oss << i << " " << attempts << std::endl;
		//		Logger::WriteMessage(oss.str().c_str());
		//	}
		//}
	};
}