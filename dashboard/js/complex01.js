function rand(min, max)
{
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

async function fetch_wallet_destination(private_key)
{
    var url = 'http://127.0.0.1:1248/query_wallet_destination?private_key=' + private_key;
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

async function add_miner_click(e)
{
    var wallet_destination = parseInt(e.target.wallet_destination);
    var count = parseInt(e.target.count);
    var url = 'http://127.0.0.1:1248/add_miner?wallet_destination=' + wallet_destination + "&count=" + count;
    const response = await fetch(url);
}

var _wallets = [];

function update_created_wallets()
{
    var ul_root = document.createElement('ul');

    for (var i in _wallets)
    {
        var [private_key, wallet_destination] = _wallets[i];

        var li_line = document.createElement("li");

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("private_key");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(private_key);
        $(li).appendTo(ul);
        $(ul).appendTo(li_line);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("wallet_destination");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(wallet_destination);
        $(li).appendTo(ul);
        $(ul).appendTo(li_line);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("funds");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(0);
        li.id = "_response_wallet_" + wallet_destination;
        $(li).appendTo(ul);
        $(ul).appendTo(li_line);

        var add_miner = document.createElement("button");
        add_miner.wallet_destination = wallet_destination;
        add_miner.count = 1;
        add_miner.addEventListener("click", add_miner_click);
        $(add_miner).html("add miner +1");
        $(add_miner).appendTo(li_line);

        var add_miner = document.createElement("button");
        add_miner.wallet_destination = wallet_destination;
        add_miner.count = 10;
        add_miner.addEventListener("click", add_miner_click);
        $(add_miner).html("add miner +10");
        $(add_miner).appendTo(li_line);

        var add_miner = document.createElement("button");
        add_miner.wallet_destination = wallet_destination;
        add_miner.count = 100;
        add_miner.addEventListener("click", add_miner_click);
        $(add_miner).html("add miner +100");
        $(add_miner).appendTo(li_line);

        $(li_line).appendTo(ul_root);

    }

    var _response_wallets = $('#_response_wallets');
    _response_wallets.empty();
    $(ul_root).appendTo(_response_wallets);
}

async function create_wallet_click()
{
    var private_key = rand(1, 0xffff);
    var wallet_destination_data = await fetch_wallet_destination(private_key);
    var wallet_destination = wallet_destination_data._wallet_destination;
    _wallets.push([private_key, wallet_destination]);
    update_created_wallets();
}

var _monitor_update_wallet_data = null;

async function fetch_consensus()
{
    var url = 'http://127.0.0.1:1248/query_consensus';
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

function find_funds(consensus_data, wallet_destination)
{
    if (null == consensus_data)
        return 0;
    var funds = 0;
    for (var i in consensus_data.consensus[0].wallet_funds)
    {
        var wallet_fund = consensus_data.consensus[0].wallet_funds[i];
        if (wallet_destination == wallet_fund.wallet)
        {
            funds = wallet_fund.amount;
            break;
        }
    }
    return funds;
}

async function update_wallet_data()
{
    clearInterval(_monitor_update_wallet_data);
    var consensus_data = null;
    try
    {
        consensus_data = await fetch_consensus();
    }
    catch(e)
    {}
    try
    {
        for (var i in _wallets)
        {
            var [ _, wallet_destination] = _wallets[i];
            var funds = find_funds(consensus_data, wallet_destination);
            var li = document.getElementById("_response_wallet_" + wallet_destination);
            $(li).html(funds);
        }
    }        
    catch(e)
    {
        console.log(e);
    }
    _monitor_update_wallet_data = setInterval(update_wallet_data, 1000);
}

async function inject_transaction_click()
{
    var source_private_key = document.getElementById("source_private_key-cta").value;
    var wallet_destination = document.getElementById("wallet_destination-cta").value;
    var amount = document.getElementById("amount-cta").value;
    var fee = document.getElementById("fee-cta").value;
    try
    {
        var url = 'http://127.0.0.1:1248/add_transaction?source_privatekey=' + source_private_key
                    + '&destination=' + wallet_destination
                    + '&amount=' + amount
                    + '&fee=' + fee;
        const response = await fetch(url);
        const data = await response.json();
        if (undefined == data)
            throw("empty data");
        if (undefined != data.message)
            throw(data.message);
    }
    catch(e)
    {
        console.log(e);
    }
}


$(document).ready(() =>
{
    document.getElementById("create_wallet-cta").addEventListener("click", create_wallet_click);
    document.getElementById("inject_transaction-cta").addEventListener("click", inject_transaction_click);
    create_wallet_click();
    update_wallet_data();
});