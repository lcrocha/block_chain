async function fetch_consensus()
{
    var url = 'http://127.0.0.1:1248/query_consensus';
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

var _monitor_consensus_update = null;

async function consensus_update()
{
    clearInterval(_monitor_consensus_update);
    //setting labels
    try
    {
        var ul_root = document.createElement('ul');
        var data = await fetch_consensus();
        var length = data.consensus.length;
        if (0 == length)
            throw "no pending transactions";
        for (var i = 0; i < length; i++)
        {
            var consensus = data.consensus[i];
            var li_line = document.createElement("li");

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("block_difficulty");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(consensus.block_difficulty);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("block_digest");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(consensus.block_digest);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("block_number");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(consensus.block_number);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("nodes_count");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(consensus.nodes_count);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("wallet_funds_count");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(consensus.wallet_funds.length);
            $(li).appendTo(ul);

            var li_funds_root = document.createElement("li");
            for (var i in consensus.wallet_funds)
            {
                var wallet_fund = consensus.wallet_funds[i];
                var ul_local = document.createElement("ul");

                var li_local = document.createElement("li");
                $(li_local).html(i);
                $(li_local).appendTo(ul_local);

                var li_local = document.createElement("li");
                $(li_local).html("wallet");
                $(li_local).appendTo(ul_local);
                var li_local = document.createElement("li");
                $(li_local).html(wallet_fund.wallet);
                $(li_local).appendTo(ul_local);

                var li_local = document.createElement("li");
                $(li_local).html("amount");
                $(li_local).appendTo(ul_local);
                var li_local = document.createElement("li");
                $(li_local).html(wallet_fund.amount);

                $(li_local).appendTo(ul_local);
                $(ul_local).appendTo(li_funds_root);
            }
            $(li_funds_root).appendTo(ul);
            $(ul).appendTo(li_line);

            $(li_line).appendTo(ul_root);
        }    
    }        
    catch(e)
    {
        var h = document.createElement("h2");
        $(h).html(e);
        $(h).appendTo(ul_root);
    }
    var response_consensus = $('#_response_consensus');
    response_consensus.empty();
    $(ul_root.innerHTML).appendTo(response_consensus);

    _monitor_consensus_update = setInterval(consensus_update, 1000);
}

$(document).ready(() =>
{
    consensus_update();
});