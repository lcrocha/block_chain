var _last_block_number = 0;

async function fetch_block(block_number)
{
    if (0 == block_number)
        block_number = "";
    var url = 'http://127.0.0.1:1248/query_block_full?block_number=' + block_number;
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

async function block_explorer_update(block_number)
{
    try
    {
        var data = await fetch_block(block_number);
        _last_block_number = data.block_number;

        var root = document.createElement("div");

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("block number");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.block_number);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("last block digest");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.last_block_digest);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("block digest");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.block_digest);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("difficulty");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.difficulty);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("tick");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.tick);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
        var li = document.createElement("li");
        $(li).html("nonce");
        $(li).appendTo(ul);
        var li = document.createElement("li");
        $(li).html(data.nonce);
        $(li).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
            var li0 = document.createElement("li");
                $(li0).html("genesis transaction");
            $(li0).appendTo(ul);

            var li0 = document.createElement("li");
                var ul1 = document.createElement("ul");
                var li1 = document.createElement("li");
                $(li1).html("wallet destination");
                $(li1).appendTo(ul1);
                var li1 = document.createElement("li");
                $(li1).html(data.genesis_transaction.wallet_destination);
                $(li1).appendTo(ul1);
                $(ul1).appendTo(li0);
            $(li0).appendTo(ul);

            var li0 = document.createElement("li");
                var ul1 = document.createElement("ul");
                var li1 = document.createElement("li");
                $(li1).html("amount");
                $(li1).appendTo(ul1);
                var li1 = document.createElement("li");
                $(li1).html(data.genesis_transaction.amount);
                $(li1).appendTo(ul1);
                $(ul1).appendTo(li0);
            $(li0).appendTo(ul);
        $(ul).appendTo(root);

        var ul = document.createElement("ul");
            var li0 = document.createElement("li");
                $(li0).html("transactions: " + data.transactions.length);
            $(li0).appendTo(ul);

            var li0 = document.createElement("li");
                var ul1 = document.createElement("ul");
                    for (tid = 0; tid < data.transactions.length; tid++)
                    {
                        var transaction = data.transactions[tid];

                        var li1 = document.createElement("li");
                            var ul2 = document.createElement("ul");

                                var li2 = document.createElement("li");
                                $(li2).html("wallet source");
                                $(li2).appendTo(ul2);
                                var li2 = document.createElement("li");
                                $(li2).html(transaction.wallet_source);
                                $(li2).appendTo(ul2);

                                var li2 = document.createElement("li");
                                $(li2).html("wallet destination");
                                $(li2).appendTo(ul2);
                                var li2 = document.createElement("li");
                                $(li2).html(transaction.wallet_destination);
                                $(li2).appendTo(ul2);

                                var li2 = document.createElement("li");
                                $(li2).html("amount");
                                $(li2).appendTo(ul2);
                                var li2 = document.createElement("li");
                                $(li2).html(transaction.amount);
                                $(li2).appendTo(ul2);

                                var li2 = document.createElement("li");
                                $(li2).html("fee");
                                $(li2).appendTo(ul2);
                                var li2 = document.createElement("li");
                                $(li2).html(transaction.fee);
                                $(li2).appendTo(ul2);

                            $(ul2).appendTo(li1);
                        $(li1).appendTo(ul1);
                    }
                $(ul1).appendTo(li0);
            $(li0).appendTo(ul);
        $(ul).appendTo(root);
    }        
    catch(e)
    {
        root = document.createElement('div');
        var h = document.createElement("h2");
        $(h).html(e);
        $(h).appendTo(root);
    }
    var response_block_explorer = $('#_response_block_explorer');
    response_block_explorer.empty();
    $(root.innerHTML).appendTo(_response_block_explorer);
}

function update()
{
    var block_number = document.getElementById("block_number-cta");
    $(block_number).val(_last_block_number);
}

async function first_click()
{
    await block_explorer_update(1);
    update();
}

async function previous_click()
{
    await block_explorer_update(_last_block_number - 1);
    update();
}

async function next_click()
{
    await block_explorer_update(_last_block_number + 1);
    update();
}

async function last_click()
{
    await block_explorer_update(-1);
    update();
}

async function go_click()
{
    var block_number_cta = document.getElementById("block_number-cta");
    var block_number = parseInt(block_number_cta.value);
    await block_explorer_update(block_number);
    update();
}

$(document).ready(() =>
{
    document.getElementById("first-cta").addEventListener("click", first_click);
    document.getElementById("previous-cta").addEventListener("click", previous_click);
    document.getElementById("next-cta").addEventListener("click", next_click);
    document.getElementById("last-cta").addEventListener("click", last_click);
    document.getElementById("go-cta").addEventListener("click", go_click);
    
    last_click();
});