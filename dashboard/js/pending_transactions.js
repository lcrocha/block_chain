async function fetch_penging_transactions()
{
    var url = 'http://127.0.0.1:1248/query_pending_transactions?block_number=';
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

var _monitor_pending_transactions_update = null;

async function pending_transactions_update()
{
    clearInterval(_monitor_pending_transactions_update);
    try
    {
        var ul_root = document.createElement('ul');
        var data = await fetch_penging_transactions();
        var length = data.transactions.length;
        if (0 == length)
            throw "no pending transactions";
        for (var i = 0; i < length; i++)
        {
            var transaction = data.transactions[i];
            var li_line = document.createElement("li");

            var label = document.createElement('label');
            $(label).html(i);
            $(label).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("source");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(transaction.wallet_source);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("destination");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(transaction.wallet_destination);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("amount");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(transaction.amount);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("fee");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(transaction.fee);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            $(li_line).appendTo(ul_root);
        }    
    }        
    catch(e)
    {
        var h = document.createElement("h2");
        $(h).html(e);
        $(h).appendTo(ul_root);
    }
    var response_pending_transactions = $('#_response_pending_transactions');
    response_pending_transactions.empty();
    $(ul_root.innerHTML).appendTo(response_pending_transactions);

    _monitor_pending_transactions_update = setInterval(pending_transactions_update, 1000);
}

$(document).ready(() =>
{
    pending_transactions_update();
});