$(document).ready(() => {
    //import {blocks_explorer_stop} from './blocks_explorer.js';
    //import {blocks_explorer_start} from './blocks_explorer.js';

    // import {blocks_explorer_start} from './blocks_explorer.js';
    // import {blocks_explorer_stop} from './blocks_explorer.js';

    // var monitor_task = setInterval(async function () {
    //     const response = await fetch('http://127.0.0.1:1248/query_nodes');
    //     const data = await response.json();
    //     parse_json(data);

    // }, 1000);

    var dashboard_ctr = document.getElementById('dashboard_sel');
    var nodes_ctr = document.getElementById('nodes_sel');
    var nodes_nav = document.getElementById('nodes_nav');
    var transactions_ctr = document.getElementById('transactions_sel');
    var blocks_ctr = document.getElementById('blocks_sel');
    var blocks_nav = document.getElementById('blocks_nav');

    function clear()
    {
        dashboard_ctr.removeAttribute("class");
        nodes_ctr.removeAttribute("class");
        nodes_nav.style.display = "none";
        transactions_ctr.removeAttribute("class");
        blocks_ctr.removeAttribute("class");
        blocks_nav.style.display = "none";
        blocks_explorer_stop();
    }

    dashboard_ctr.addEventListener("click", () =>
    {
        clear();
        $(dashboard_ctr).addClass("current");
    });

    nodes_ctr.addEventListener("click", () =>
    {
        clear();
        $(nodes_ctr).addClass("current");
        nodes_nav.style.display = "flex";
    });

    transactions_ctr.addEventListener("click", () =>
    {
        clear();
        $(transactions_ctr).addClass("current");
    });

    blocks_ctr.addEventListener("click", () =>
    {
        clear();
        $(blocks_ctr).addClass("current");
        blocks_nav.style.display = "flex";
        blocks_explorer_start();
    });

    blocks_ctr.click();


    // function parse_json(data)
    // {
    //     if (data.nodes.length <= 0) return;
    //     var response = $("#_response_nodes");
    //     response.empty();
    //     for (var node in data.nodes)
    //     {
    //         var new_card = create_card(data.nodes[node]);
    //         $(new_card).appendTo(response);
    //     }
    // }

    // function create_row() {
    //     let newRow = document.createElement("div");
    //     $(newRow).addClass("row");

    //     return newRow;
    // }

    // function create_card(data)
    // {
    //     var li = document.createElement("li");
    //     var div0 = document.createElement("div");
    //     var div1 = document.createElement("div");

    //     //card div body
    //     var new_card_div_body = document.createElement("div");
    //     $(new_card_div_body).addClass("card-body");

    //     //card title
    //     var new_title = document.createElement("p");
    //     $(new_title).addClass("card-title");
    //     $(new_title).html("node_id: " + data.node_id);
    //     $(new_title).appendTo(new_card_div_body);

    //     var new_paragraph = document.createElement("p");
    //     $(new_paragraph).addClass("card-text");
    //     $(new_paragraph).html("is_mining: " + data.node_is_mining);
    //     $(new_paragraph).appendTo(new_card_div_body);

    //     var new_paragraph2 = document.createElement("p");
    //     $(new_paragraph2).addClass("card-text");
    //     $(new_paragraph2).html("last_block_number: " + data.last_block_number);
    //     $(new_paragraph2).appendTo(new_card_div_body);

    //     var new_paragraph2 = document.createElement("p");
    //     $(new_paragraph2).addClass("card-text");
    //     $(new_paragraph2).html("blocks_count: " + data.blocks_count);
    //     $(new_paragraph2).appendTo(new_card_div_body);

    //     var new_paragraph = document.createElement("p");
    //     $(new_paragraph).addClass("card-text");
    //     $(new_paragraph).html("last_block_digest: " + data.last_block_digest.toString());
    //     $(new_paragraph).appendTo(new_card_div_body);

    //     var new_paragraph3 = document.createElement("p");
    //     $(new_paragraph3).addClass("card-text");
    //     $(new_paragraph3).html("difficulty: " + data.difficulty.toString());
    //     $(new_paragraph3).appendTo(new_card_div_body);

    //     var new_paragraph3 = document.createElement("p");
    //     $(new_paragraph3).addClass("card-text");
    //     $(new_paragraph3).html("difficulty_change: " + data.difficulty_change.toFixed(4));
    //     $(new_paragraph3).appendTo(new_card_div_body);

    //     $(new_card_div_body).appendTo(div1);
    //     $(div1).appendTo(div0);
    //     $(div0).appendTo(li);

    //     return li;
    // }
});