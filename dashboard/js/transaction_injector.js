async function fetch_penging_transactions()
{
    var url = 'http://127.0.0.1:1248/query_pending_transactions?block_number=';
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

async function fetch_processor()
{
    var url = 'http://127.0.0.1:1248/query_processor';
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

var _monitor_transaction_injector_update = null;

async function transaction_injector_update()
{
    clearInterval(_monitor_transaction_injector_update);
    //setting labels
    try
    {
        var ul_root = document.createElement('ul');
        var penging_transactions_data = await fetch_penging_transactions();
        var pending_transactions = parseInt(penging_transactions_data.transactions.length);
        var processor_data = await fetch_processor();
        var processor_transactions = parseInt(processor_data.processed_count) + parseInt(processor_data.to_process_count);

        var current_transactions = pending_transactions + processor_transactions;
        for (var i = current_transactions; i < _max_pending_transactions; i++)
        {

            console.log(i);
        }
        var date = new Date();
        console.log("--" + date.getTime());
    }        
    catch(e)
    {
        var h = document.createElement("h2");
        $(h).html(e);
        $(h).appendTo(ul_root);
    }
    _monitor_transaction_injector_update = setInterval(transaction_injector_update, 1000);
}

var _running = false;
var _max_pending_transactions = 10;

function update()
{
    var label = document.getElementById("status-cta");
    $(label).html(_running ? "running" : "stopped");

    var input = document.getElementById("max_pending_transactions-cta");
    $(input).val(parseInt(_max_pending_transactions));
}

function start_click()
{
    if (_running)
        return;
    _running = true;
    //parsing max_pending_transactions
    _max_pending_transactions = parseInt(document.getElementById("max_pending_transactions-cta").value);
    if (_max_pending_transactions < 1)
        _max_pending_transactions = 1;
    update();
    transaction_injector_update();
}

function stop_click()
{
    if (!_running)
        return;
    _running = false;
    update();
}

$(document).ready(() =>
{
    update();
    document.getElementById("start-cta").addEventListener("click", start_click);
    document.getElementById("stop-cta").addEventListener("click", stop_click);
    start_click();
});