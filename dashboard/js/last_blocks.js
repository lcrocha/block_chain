async function fetch_block(block_number)
{
    if (0 == block_number)
        block_number = "";
    var url = 'http://127.0.0.1:1248/query_block_no_transactions?block_number=' + block_number;
    const response = await fetch(url);
    const data = await response.json();
    if (undefined == data)
        throw("empty data");
    if (undefined != data.message)
        throw(data.message);

    return data;
}

var _monitor_last_blocks_update = null;
var _last_block_number = 0;

async function last_blocks_update()
{
    clearInterval(_monitor_last_blocks_update);
    //setting labels
    try
    {
        var update = false;
        var i = 0;
        var block_number = 0;
        var ul_root = document.createElement('ul');
        while (i++ < 5)
        {
            var data = await fetch_block(block_number);
            if (0 == block_number)
            {
                if (_last_block_number == data.block_number)
                    break;
                _last_block_number = data.block_number;
                update = true;
            }
            block_number = data.block_number - 1;

            var li_line = document.createElement("li");

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("block number");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.block_number);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("last block digest");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.last_block_digest);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("block digest");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.block_digest);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("difficulty");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.difficulty);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("tick");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.tick);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("nonce");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.nonce);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("genesis transaction amount");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.genesis_transaction.amount);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);

            var ul = document.createElement("ul");
            var li = document.createElement("li");
            $(li).html("transactions_count");
            $(li).appendTo(ul);
            var li = document.createElement("li");
            $(li).html(data.transactions_count);
            $(li).appendTo(ul);
            $(ul).appendTo(li_line);
            $(li_line).appendTo(ul_root);
        }
    }        
    catch(e)
    {
        var h = document.createElement("h2");
        $(h).html(e);
        $(h).appendTo(ul_root);
        update = true;
    }
    if (update)
    {
        var response_last_blocks = $('#_response_last_blocks');
        response_last_blocks.empty();
        $(ul_root.innerHTML).appendTo(response_last_blocks);
    }    
    _monitor_last_blocks_update = setInterval(last_blocks_update, 1000);
}

$(document).ready(() =>
{
    last_blocks_update();
});